<?php

class VBook {
  protected $cards = array();

  public function addCard(VCard $card){
    $this->cards[] = $card;
  }

  public function toString() {
    $cards = array();
    foreach($this->cards as $card) {
      $cards[] = $card->toString();
    }
    return join("\n", $cards);
  }

  public static function fromString($string) {
    $book = new VBook;
    foreach (VCard::fromString($string) as $card) {
      $book->addCard($card);
    }
    return $book;
  }
}

class VCard {

  protected $version = 2.1;
  protected $properties = array();

  function __construct($version = 2.1) {
    $this->version = $version;
  }

  public function addProperty($type, $values = NULL, $subtypes = array()) {
    $this->properties[] = is_a($type, "VCardProperty") ?
        $type :
        new VCardProperty($type, $values, $subtypes);
  }

  public function getProperties($type) {
    $properties = array();
    foreach ($this->properties as $property) {
      if ($property->type() == $type) {
        $properties[] = $property;
      }
    }
    return $properties;
  }

  public function getProperty($type) {
    $this->getProperties($type);
  }

  public function toString() {
    $lines = array();
    $lines[] = "BEGIN:VCARD";

    foreach($this->properties as $property) {
      $lines[] = $property->toString();
    }

    $version = $this->versionProperty();
    if (empty($version)) {
      $lines[] = "VERSION:{$this->version}";
    }

    $lines[] = "END:VCARD";
    return join("\n", $lines);
  }

  public function versionProperty() {
    return $this->getProperty('VERSION');
  }

  public static function fromString($string){
    $card = NULL;
    $cards = array();
    $lines = preg_split("/(!<)\n/", $string);

    foreach($lines as $i => $line) {
      if(preg_match("/BEGIN:VCARD/", $line)){
        if($card){
          throw new Exception("Found new vCard declaration, expected an end first.");
        }

        $card = new VCard;
      }
      else {
        if (!$card) {
          throw new Exception("Found end of vCard, expected beginning first.");
        }

        if(preg_match("/END:VCARD/", $line)) {
          $cards[] = $card;
          $card = NULL;
        }
        else {
          $card->addProperty(VCardProperty::fromString($line));
        }
      }
    }

    if (!$card) {
      throw new Exception("Expected an end of vCard, EOF found.");
    }

    return $cards;
  }
}

class VCardProperty {

  protected $type;
  protected $subtypes = array();
  protected $values = array();

  public function __construct($type, $values, $subtypes = array()) {
    $this->type = $type;
    $this->subtypes = is_array($subtypes) ? $subtypes : array($subtypes);
    $this->setValue($values) ? $values : array($values);
  }

  public function type() {
    return $this->type;
  }

  public function setValue($values){
    $this->values = is_array($values) ? $values : array($values);
  }

  public function toString() {
    $types = array_merge(array($this->type), $this->subtypes);
    return join(";", $types) . ":" . join(";", $this->values);
  }

  public function isValid(){
  }

  public function isEmpty(){
    empty($this->values);
  }

  public static function fromString($string) {
    list($types, $values) = preg_split("/(!<[\])[:]/", $string);

    $types = preg_split("/(!<[\])[;]/", $types);
    $values = preg_split("/(!<[\])[;]/", $values);

    return new VCardProperty(array_shift($types), $values, $types);
  }
}