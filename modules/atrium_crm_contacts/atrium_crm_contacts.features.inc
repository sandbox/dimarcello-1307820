<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function atrium_crm_contacts_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => 3);
  }
  elseif ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function atrium_crm_contacts_node_info() {
  $items = array(
    'atrium_crm_contact' => array(
      'name'           => t('Contact'),
      'module'         => 'atrium_crm_contacts',
      'description'    => t('A contact is a person of interest belonging to one or more accounts.'),
      'has_title'      => '1',
      'title_label'    => t('Full name'),
      'has_body'       => '0',
      'body_label'     => '',
      'min_word_count' => '0',
      'help'           => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function atrium_crm_contacts_views_api() {
  return array(
    'api' => '2',
  );
}
