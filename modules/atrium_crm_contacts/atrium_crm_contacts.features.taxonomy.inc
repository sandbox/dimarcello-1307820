<?php

/**
 * Implementation of hook_taxonomy_default_vocabularies().
 */
function atrium_crm_contacts_taxonomy_default_vocabularies() {
  return array(
    'atrium_crm_contact_type' => array(
      'name'        => 'Contact type',
      'description' => '',
      'help'        => '',
      'relations'   => '1',
      'hierarchy'   => '0',
      'multiple'    => '0',
      'required'    => '0',
      'tags'        => '1',
      'module'      => 'features_atrium_crm_contact_type',
      'weight'      => '0',
      'nodes'       => array(
        'atrium_crm_contact' => 'atrium_crm_contact',
      ),
    ),
  );
}
