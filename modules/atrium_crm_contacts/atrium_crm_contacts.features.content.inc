<?php

/**
 * Implementation of hook_content_default_fields().
 */
function atrium_crm_contacts_content_default_fields() {
  $fields = array();

  // Exported field: field_contact_accounts
  $fields['atrium_crm_contact-field_contact_accounts'] = array(
    'field_name'          => 'field_contact_accounts',
    'type_name'           => 'atrium_crm_contact',
    'display_settings'    => array(
      'weight' => 0,
      'parent' => 'group_contact_miscellaneous',
      'label'  => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'full'   => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '5'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '4'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '2'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '3'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'token'  => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active'       => '1',
    'type'                => 'nodereference',
    'required'            => '0',
    'multiple'            => '1',
    'module'              => 'nodereference',
    'active'              => '1',
    'referenceable_types' => array(
      'atrium_crm_account'        => 'atrium_crm_account',
      'blog'                      => 0,
      'book'                      => 0,
      'casetracker_basic_case'    => 0,
      'atrium_crm_contact'        => 0,
      'event'                     => 0,
      'group'                     => 0,
      'profile'                   => 0,
      'casetracker_basic_project' => 0,
      'shoutbox'                  => 0,
      'feed_ical_item'            => 0,
      'feed_ical'                 => 0,
    ),
    'advanced_view'       => '--',
    'advanced_view_args'  => '',
    'widget'              => array(
      'autocomplete_match' => 'contains',
      'size'               => '60',
      'default_value'      => array(
        '0' => array(
          'nid'            => NULL,
          '_error_element' => 'default_value_widget][field_contact_accounts][0][nid][nid',
        ),
      ),
      'default_value_php'  => NULL,
      'label'              => 'Accounts',
      'weight'             => 0,
      'description'        => '',
      'type'               => 'nodereference_autocomplete',
      'module'             => 'nodereference',
    ),
  );

  // Exported field: field_contact_addresses
  $fields['atrium_crm_contact-field_contact_addresses'] = array(
    'field_name'        => 'field_contact_addresses',
    'type_name'         => 'atrium_crm_contact',
    'display_settings'  => array(
      'weight' => '2',
      'parent' => 'group_contact_contact',
      'label'  => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'full'   => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '5'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '4'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '2'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '3'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'token'  => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active'     => '1',
    'type'              => 'location',
    'required'          => '0',
    'multiple'          => '1',
    'module'            => 'location_cck',
    'active'            => '1',
    'location_settings' => array(
      'form'    => array(
        'fields' => array(
          'name'        => array(
            'collect' => '1',
            'default' => '',
            'weight'  => '2',
          ),
          'street'      => array(
            'collect' => '2',
            'default' => '',
            'weight'  => '4',
          ),
          'additional'  => array(
            'collect' => '1',
            'default' => '',
            'weight'  => '6',
          ),
          'city'        => array(
            'collect' => '2',
            'default' => '',
            'weight'  => '8',
          ),
          'province'    => array(
            'collect' => '1',
            'default' => '',
            'weight'  => '10',
          ),
          'postal_code' => array(
            'collect' => '2',
            'default' => '',
            'weight'  => '12',
          ),
          'country'     => array(
            'collect' => '1',
            'default' => 'nl',
            'weight'  => '14',
          ),
          'locpick'     => array(
            'collect' => '0',
            'weight'  => '20',
          ),
        ),
      ),
      'display' => array(
        'hide' => array(
          'name'          => 0,
          'street'        => 0,
          'additional'    => 0,
          'city'          => 0,
          'province'      => 0,
          'postal_code'   => 0,
          'country'       => 0,
          'locpick'       => 0,
          'province_name' => 0,
          'country_name'  => 0,
          'map_link'      => 0,
          'coords'        => 0,
        ),
      ),
    ),
    'gmap_macro'        => '[gmap ]',
    'gmap_marker'       => 'drupal',
    'widget'            => array(
      'default_value'     => array(
        '0' => array(
          'name'                    => '',
          'street'                  => '',
          'additional'              => '',
          'country'                 => 'us',
          'locpick'                 => array(
            'user_latitude'  => '',
            'user_longitude' => '',
          ),
          'cck_preview_in_progress' => TRUE,
          'location_settings'       => array(
            'form' => array(
              'fields' => array(
                'lid'             => array(
                  'default' => FALSE,
                ),
                'name'            => array(
                  'default' => '',
                  'collect' => 1,
                  'weight'  => 2,
                ),
                'street'          => array(
                  'default' => '',
                  'collect' => 1,
                  'weight'  => 4,
                ),
                'additional'      => array(
                  'default' => '',
                  'collect' => 1,
                  'weight'  => 6,
                ),
                'city'            => array(
                  'default' => '',
                  'collect' => 0,
                  'weight'  => 8,
                ),
                'province'        => array(
                  'default' => '',
                  'collect' => 0,
                  'weight'  => 10,
                ),
                'postal_code'     => array(
                  'default' => '',
                  'collect' => 0,
                  'weight'  => 12,
                ),
                'country'         => array(
                  'default' => 'us',
                  'collect' => 1,
                  'weight'  => 14,
                ),
                'locpick'         => array(
                  'default' => FALSE,
                  'collect' => 1,
                  'weight'  => 20,
                  'nodiff'  => TRUE,
                ),
                'latitude'        => array(
                  'default' => 0,
                ),
                'longitude'       => array(
                  'default' => 0,
                ),
                'source'          => array(
                  'default' => 0,
                ),
                'is_primary'      => array(
                  'default' => 0,
                ),
                'delete_location' => array(
                  'default' => FALSE,
                  'nodiff'  => TRUE,
                ),
              ),
            ),
          ),
        ),
      ),
      'default_value_php' => NULL,
      'label'             => 'Addresses',
      'weight'            => '2',
      'description'       => '',
      'type'              => 'location',
      'module'            => 'location_cck',
    ),
  );

  // Exported field: field_contact_date_of_birth
  $fields['atrium_crm_contact-field_contact_date_of_birth'] = array(
    'field_name'       => 'field_contact_date_of_birth',
    'type_name'        => 'atrium_crm_contact',
    'display_settings' => array(
      'weight' => '8',
      'parent' => 'group_crm_personal',
      'label'  => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'full'   => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '5'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '4'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '2'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '3'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'token'  => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active'    => '1',
    'type'             => 'date',
    'required'         => '0',
    'multiple'         => '0',
    'module'           => 'date',
    'active'           => '1',
    'granularity'      => array(
      'year'  => 'year',
      'month' => 'month',
      'day'   => 'day',
    ),
    'timezone_db'      => '',
    'tz_handling'      => 'none',
    'todate'           => '',
    'repeat'           => 0,
    'repeat_collapsed' => '',
    'default_format'   => 'medium',
    'widget'           => array(
      'default_value'       => 'blank',
      'default_value_code'  => '',
      'default_value2'      => 'same',
      'default_value_code2' => '',
      'input_format'        => 'j F Y',
      'input_format_custom' => '',
      'increment'           => '1',
      'text_parts'          => array(),
      'year_range'          => '-100:+0',
      'label_position'      => 'above',
      'label'               => 'Date of birth',
      'weight'              => '8',
      'description'         => '',
      'type'                => 'date_popup',
      'module'              => 'date',
    ),
  );

  // Exported field: field_contact_email_addresses
  $fields['atrium_crm_contact-field_contact_email_addresses'] = array(
    'field_name'       => 'field_contact_email_addresses',
    'type_name'        => 'atrium_crm_contact',
    'display_settings' => array(
      'weight' => '3',
      'parent' => 'group_contact_contact',
      'label'  => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'full'   => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '5'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '4'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '2'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '3'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'token'  => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active'    => '1',
    'type'             => 'email',
    'required'         => '0',
    'multiple'         => '1',
    'module'           => 'email',
    'active'           => '1',
    'widget'           => array(
      'size'              => '60',
      'default_value'     => array(
        '0' => array(
          'email' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label'             => 'Email addresses',
      'weight'            => '3',
      'description'       => '',
      'type'              => 'email_textfield',
      'module'            => 'email',
    ),
  );

  // Exported field: field_contact_gender
  $fields['atrium_crm_contact-field_contact_gender'] = array(
    'field_name'         => 'field_contact_gender',
    'type_name'          => 'atrium_crm_contact',
    'display_settings'   => array(
      'weight' => '9',
      'parent' => 'group_crm_personal',
      'label'  => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'full'   => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '5'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '4'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '2'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '3'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'token'  => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active'      => '1',
    'type'               => 'text',
    'required'           => '0',
    'multiple'           => '0',
    'module'             => 'text',
    'active'             => '1',
    'text_processing'    => '0',
    'max_length'         => '1',
    'allowed_values'     => 'm|Male
f|Female',
    'allowed_values_php' => '',
    'widget'             => array(
      'default_value'     => array(
        '0' => array(
          'value' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label'             => 'Gender',
      'weight'            => '9',
      'description'       => '',
      'type'              => 'optionwidgets_buttons',
      'module'            => 'optionwidgets',
    ),
  );

  // Exported field: field_contact_given_name
  $fields['atrium_crm_contact-field_contact_given_name'] = array(
    'field_name'         => 'field_contact_given_name',
    'type_name'          => 'atrium_crm_contact',
    'display_settings'   => array(
      'weight' => '6',
      'parent' => 'group_crm_personal',
      'label'  => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'full'   => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '5'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '4'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '2'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '3'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'token'  => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active'      => '1',
    'type'               => 'text',
    'required'           => '1',
    'multiple'           => '0',
    'module'             => 'text',
    'active'             => '1',
    'text_processing'    => '0',
    'max_length'         => '',
    'allowed_values'     => '',
    'allowed_values_php' => '',
    'widget'             => array(
      'rows'              => 5,
      'size'              => '60',
      'default_value'     => array(
        '0' => array(
          'value'          => '',
          '_error_element' => 'default_value_widget][field_contact_given_name][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label'             => 'Given name',
      'weight'            => '6',
      'description'       => '',
      'type'              => 'text_textfield',
      'module'            => 'text',
    ),
  );

  // Exported field: field_contact_member
  $fields['atrium_crm_contact-field_contact_member'] = array(
    'field_name'           => 'field_contact_member',
    'type_name'            => 'atrium_crm_contact',
    'display_settings'     => array(
      'weight' => '-1',
      'parent' => 'group_contact_miscellaneous',
      'label'  => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'full'   => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '5'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '4'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '2'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '3'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'token'  => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active'        => '1',
    'type'                 => 'userreference',
    'required'             => '0',
    'multiple'             => '0',
    'module'               => 'userreference',
    'active'               => '1',
    'referenceable_roles'  => array(
      '2' => 0,
      '5' => 0,
      '3' => 0,
      '4' => 0,
    ),
    'referenceable_status' => '',
    'advanced_view'        => '--',
    'advanced_view_args'   => '',
    'widget'               => array(
      'autocomplete_match' => 'contains',
      'size'               => 60,
      'reverse_link'       => 1,
      'default_value'      => array(
        '0' => array(
          'uid' => '',
        ),
      ),
      'default_value_php'  => NULL,
      'label'              => 'Attached to Member',
      'weight'             => '-1',
      'description'        => '',
      'type'               => 'userreference_select',
      'module'             => 'userreference',
    ),
  );

  // Exported field: field_contact_notes
  $fields['atrium_crm_contact-field_contact_notes'] = array(
    'field_name'         => 'field_contact_notes',
    'type_name'          => 'atrium_crm_contact',
    'display_settings'   => array(
      'weight' => '-2',
      'parent' => 'group_contact_miscellaneous',
      'label'  => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'full'   => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '5'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '4'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '2'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '3'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'token'  => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active'      => '1',
    'type'               => 'text',
    'required'           => '0',
    'multiple'           => '0',
    'module'             => 'text',
    'active'             => '1',
    'text_processing'    => '0',
    'max_length'         => '',
    'allowed_values'     => '',
    'allowed_values_php' => '',
    'widget'             => array(
      'rows'              => '5',
      'size'              => 60,
      'default_value'     => array(
        '0' => array(
          'value'          => '',
          '_error_element' => 'default_value_widget][field_contact_notes][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label'             => 'Notes',
      'weight'            => '-2',
      'description'       => '',
      'type'              => 'text_textarea',
      'module'            => 'text',
    ),
  );

  // Exported field: field_contact_opted_out
  $fields['atrium_crm_contact-field_contact_opted_out'] = array(
    'field_name'         => 'field_contact_opted_out',
    'type_name'          => 'atrium_crm_contact',
    'display_settings'   => array(
      'weight' => 0,
      'parent' => 'group_contact_opting',
      'label'  => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'full'   => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '5'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '4'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '2'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '3'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'token'  => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active'      => '1',
    'type'               => 'text',
    'required'           => '0',
    'multiple'           => '1',
    'module'             => 'text',
    'active'             => '1',
    'text_processing'    => '0',
    'max_length'         => '',
    'allowed_values'     => 'phone|Phone
mail|Mail
email|Email',
    'allowed_values_php' => '',
    'widget'             => array(
      'default_value'     => array(
        '0' => array(
          'value' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label'             => 'Opted out',
      'weight'            => 0,
      'description'       => '',
      'type'              => 'optionwidgets_buttons',
      'module'            => 'optionwidgets',
    ),
  );

  // Exported field: field_contact_phone_numbers
  $fields['atrium_crm_contact-field_contact_phone_numbers'] = array(
    'field_name'       => 'field_contact_phone_numbers',
    'type_name'        => 'atrium_crm_contact',
    'display_settings' => array(
      'weight' => '4',
      'parent' => 'group_contact_contact',
      'label'  => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'full'   => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '5'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '4'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '2'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '3'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'token'  => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active'    => '1',
    'type'             => 'text',
    'required'         => '0',
    'multiple'         => '1',
    'module'           => 'text',
    'active'           => '1',
    'text_processing'  => '',
    'max_length'       => '20',
    'allowed_values'   => '',
    'widget'           => array(
      'rows'              => 5,
      'size'              => '60',
      'default_value'     => array(
        '0' => array(
          'value'          => '',
          '_error_element' => 'default_value_widget][field_contact_phone_numbers][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label'             => 'Phone numbers',
      'weight'            => '4',
      'description'       => '',
      'type'              => 'text_textfield',
      'module'            => 'text',
    ),
  );

  // Exported field: field_contact_salutation
  $fields['atrium_crm_contact-field_contact_salutation'] = array(
    'field_name'         => 'field_contact_salutation',
    'type_name'          => 'atrium_crm_contact',
    'display_settings'   => array(
      'weight' => '5',
      'parent' => 'group_crm_personal',
      'label'  => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'full'   => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '5'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '4'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '2'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '3'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'token'  => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active'      => '1',
    'type'               => 'text',
    'required'           => '0',
    'multiple'           => '0',
    'module'             => 'text',
    'active'             => '1',
    'text_processing'    => '0',
    'max_length'         => '',
    'allowed_values'     => 'mr|Mr.
mrs|Mrs.
ms|Ms.
ir|Ir.
dr|Dr.
drs|Drs.',
    'allowed_values_php' => '',
    'widget'             => array(
      'rows'              => 5,
      'size'              => '10',
      'default_value'     => array(
        '0' => array(
          'value' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label'             => 'Salutation',
      'weight'            => '5',
      'description'       => '',
      'type'              => 'optionwidgets_select',
      'module'            => 'optionwidgets',
    ),
  );

  // Exported field: field_contact_surname
  $fields['atrium_crm_contact-field_contact_surname'] = array(
    'field_name'         => 'field_contact_surname',
    'type_name'          => 'atrium_crm_contact',
    'display_settings'   => array(
      'weight' => '7',
      'parent' => 'group_crm_personal',
      'label'  => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'full'   => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '5'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '4'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '2'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '3'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'token'  => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active'      => '1',
    'type'               => 'text',
    'required'           => '1',
    'multiple'           => '0',
    'module'             => 'text',
    'active'             => '1',
    'text_processing'    => '0',
    'max_length'         => '',
    'allowed_values'     => '',
    'allowed_values_php' => '',
    'widget'             => array(
      'rows'              => 5,
      'size'              => '60',
      'default_value'     => array(
        '0' => array(
          'value'          => '',
          '_error_element' => 'default_value_widget][field_contact_surname][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label'             => 'Surname',
      'weight'            => '7',
      'description'       => '',
      'type'              => 'text_textfield',
      'module'            => 'text',
    ),
  );

  // Exported field: field_contact_websites
  $fields['atrium_crm_contact-field_contact_websites'] = array(
    'field_name'       => 'field_contact_websites',
    'type_name'        => 'atrium_crm_contact',
    'display_settings' => array(
      'weight' => '5',
      'parent' => 'group_contact_contact',
      'label'  => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'full'   => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '5'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '4'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '2'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '3'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'token'  => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active'    => '1',
    'type'             => 'link',
    'required'         => '0',
    'multiple'         => '1',
    'module'           => 'link',
    'active'           => '1',
    'attributes'       => array(
      'target' => '_blank',
      'rel'    => '',
      'class'  => '',
      'title'  => '',
    ),
    'display'          => array(
      'url_cutoff' => '80',
    ),
    'url'              => 0,
    'title'            => 'optional',
    'title_value'      => '',
    'enable_tokens'    => 0,
    'validate_url'     => 1,
    'widget'           => array(
      'default_value'     => array(
        '0' => array(
          'title' => '',
          'url'   => '',
        ),
      ),
      'default_value_php' => NULL,
      'label'             => 'Websites',
      'weight'            => '5',
      'description'       => '',
      'type'              => 'link',
      'module'            => 'link',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Accounts');
  t('Addresses');
  t('Attached to Member');
  t('Date of birth');
  t('Email addresses');
  t('Gender');
  t('Given name');
  t('Notes');
  t('Opted out');
  t('Phone numbers');
  t('Salutation');
  t('Surname');
  t('Websites');

  return $fields;
}
