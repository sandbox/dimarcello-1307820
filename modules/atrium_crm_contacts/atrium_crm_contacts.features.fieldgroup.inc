<?php

/**
 * Implementation of hook_fieldgroup_default_groups().
 */
function atrium_crm_contacts_fieldgroup_default_groups() {
  $groups = array();

  // Exported group: group_contact_contact
  $groups['atrium_crm_contact-group_contact_contact'] = array(
    'group_type' => 'standard',
    'type_name'  => 'atrium_crm_contact',
    'group_name' => 'group_contact_contact',
    'label'      => 'Contact details',
    'settings'   => array(
      'form'    => array(
        'style'       => 'fieldset_collapsible',
        'description' => '',
      ),
      'display' => array(
        'weight'      => '-2',
        'label'       => 'above',
        'teaser'      => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        'full'        => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        'description' => '',
        '5'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        '4'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        '2'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        '3'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        'token'       => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
      ),
    ),
    'weight'     => '-2',
    'fields'     => array(
      '0' => 'field_contact_addresses',
      '1' => 'field_contact_email_addresses',
      '2' => 'field_contact_phone_numbers',
      '3' => 'field_contact_websites',
    ),
  );

  // Exported group: group_contact_miscellaneous
  $groups['atrium_crm_contact-group_contact_miscellaneous'] = array(
    'group_type' => 'standard',
    'type_name'  => 'atrium_crm_contact',
    'group_name' => 'group_contact_miscellaneous',
    'label'      => 'Miscellaneous',
    'settings'   => array(
      'form'    => array(
        'style'       => 'fieldset_collapsible',
        'description' => '',
      ),
      'display' => array(
        'weight'      => '-4',
        'label'       => 'above',
        'teaser'      => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        'full'        => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        'description' => '',
        '5'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        '4'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        '2'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        '3'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        'token'       => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
      ),
    ),
    'weight'     => '-4',
    'fields'     => array(
      '0' => 'field_contact_notes',
      '1' => 'field_contact_member',
      '2' => 'field_contact_accounts',
    ),
  );

  // Exported group: group_contact_opting
  $groups['atrium_crm_contact-group_contact_opting'] = array(
    'group_type' => 'standard',
    'type_name'  => 'atrium_crm_contact',
    'group_name' => 'group_contact_opting',
    'label'      => 'Opting details',
    'settings'   => array(
      'form'    => array(
        'style'       => 'fieldset_collapsible',
        'description' => '',
      ),
      'display' => array(
        'weight'      => '-1',
        'label'       => 'above',
        'teaser'      => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        'full'        => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        'description' => '',
        '5'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        '4'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        '2'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        '3'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        'token'       => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
      ),
    ),
    'weight'     => '-1',
    'fields'     => array(
      '0' => 'field_contact_opted_out',
    ),
  );

  // Exported group: group_crm_personal
  $groups['atrium_crm_contact-group_crm_personal'] = array(
    'group_type' => 'standard',
    'type_name'  => 'atrium_crm_contact',
    'group_name' => 'group_crm_personal',
    'label'      => 'Personal details',
    'settings'   => array(
      'form'    => array(
        'style'       => 'fieldset_collapsible',
        'description' => '',
      ),
      'display' => array(
        'weight'      => '-3',
        'label'       => 'above',
        'teaser'      => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        'full'        => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        'description' => '',
        '5'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        '4'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        '2'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        '3'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        'token'       => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
      ),
    ),
    'weight'     => '-3',
    'fields'     => array(
      '0' => 'field_contact_salutation',
      '1' => 'field_contact_given_name',
      '2' => 'field_contact_surname',
      '3' => 'field_contact_date_of_birth',
      '4' => 'field_contact_gender',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Contact details');
  t('Miscellaneous');
  t('Opting details');
  t('Personal details');

  return $groups;
}
