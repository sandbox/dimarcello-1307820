<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function atrium_crm_sets_context_default_contexts() {
  $export = array();

  $context                 = new stdClass;
  $context->disabled       = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version    = 3;
  $context->name           = 'atrium_crm_set';
  $context->description    = '';
  $context->tag            = 'Atrium CRM';
  $context->conditions     = array(
    'node' => array(
      'values'  => array(
        'atrium_crm_set' => 'atrium_crm_set',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions      = array(
    'block' => array(
      'blocks' => array(),
      'layout' => 'wide',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Atrium CRM');
  $export['atrium_crm_set'] = $context;

  $context                 = new stdClass;
  $context->disabled       = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version    = 3;
  $context->name           = 'atrium_crm_sets';
  $context->description    = '';
  $context->tag            = 'Atrium CRM';
  $context->conditions     = array(
    'views' => array(
      'values' => array(
        'atrium_crm_sets:page_1' => 'atrium_crm_sets:page_1',
      ),
    ),
  );
  $context->reactions      = array(
    'block' => array(
      'blocks' => array(
        'views--exp-atrium_crm_sets-page_1' => array(
          'module' => 'views',
          'delta'  => '-exp-atrium_crm_sets-page_1',
          'region' => 'right',
          'weight' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Atrium CRM');
  $export['atrium_crm_sets'] = $context;

  return $export;
}
