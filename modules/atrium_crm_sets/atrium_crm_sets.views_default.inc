<?php

/**
 * Implementation of hook_views_default_views().
 */
function atrium_crm_sets_views_default_views() {
  $views = array();

  // Exported view: atrium_crm_sets
  $view               = new view;
  $view->name         = 'atrium_crm_sets';
  $view->description  = 'Listing of all sets';
  $view->tag          = 'Atrium CRM';
  $view->view_php     = '';
  $view->base_table   = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version  = 2;
  $view->disabled     = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler            = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('relationships', array(
    'field_set_contacts_nid' => array(
      'label'        => 'Contacts',
      'required'     => 0,
      'delta'        => '-1',
      'id'           => 'field_set_contacts_nid',
      'table'        => 'node_data_field_set_contacts',
      'field'        => 'field_set_contacts_nid',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('fields', array(
    'title'     => array(
      'label'        => 'Title',
      'alter'        => array(
        'alter_text'    => 0,
        'text'          => '',
        'make_link'     => 0,
        'path'          => '',
        'link_class'    => '',
        'alt'           => '',
        'prefix'        => '',
        'suffix'        => '',
        'target'        => '',
        'help'          => '',
        'trim'          => 0,
        'max_length'    => '',
        'word_boundary' => 1,
        'ellipsis'      => 1,
        'html'          => 0,
        'strip_tags'    => 0,
      ),
      'empty'        => '',
      'hide_empty'   => 0,
      'empty_zero'   => 0,
      'link_to_node' => 1,
      'spaces'       => array(
        'frontpage' => 0,
        'type'      => 'spaces_og',
      ),
      'exclude'      => 0,
      'id'           => 'title',
      'table'        => 'node',
      'field'        => 'title',
      'override'     => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
    'teaser'    => array(
      'label'        => 'Description',
      'alter'        => array(
        'alter_text'    => 0,
        'text'          => '',
        'make_link'     => 0,
        'path'          => '',
        'link_class'    => '',
        'alt'           => '',
        'prefix'        => '',
        'suffix'        => '',
        'target'        => '',
        'help'          => '',
        'trim'          => 0,
        'max_length'    => '',
        'word_boundary' => 1,
        'ellipsis'      => 1,
        'html'          => 0,
        'strip_tags'    => 1,
      ),
      'empty'        => '',
      'hide_empty'   => 0,
      'empty_zero'   => 0,
      'exclude'      => 0,
      'id'           => 'teaser',
      'table'        => 'node_revisions',
      'field'        => 'teaser',
      'relationship' => 'none',
    ),
    'edit_node' => array(
      'label'        => 'Edit link',
      'alter'        => array(
        'alter_text'    => 0,
        'text'          => '',
        'make_link'     => 0,
        'path'          => '',
        'link_class'    => '',
        'alt'           => '',
        'prefix'        => '',
        'suffix'        => '',
        'target'        => '',
        'help'          => '',
        'trim'          => 0,
        'max_length'    => '',
        'word_boundary' => 1,
        'ellipsis'      => 1,
        'html'          => 0,
        'strip_tags'    => 0,
      ),
      'empty'        => '',
      'hide_empty'   => 0,
      'empty_zero'   => 0,
      'text'         => '',
      'exclude'      => 0,
      'id'           => 'edit_node',
      'table'        => 'node',
      'field'        => 'edit_node',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('filters', array(
    'type'                                      => array(
      'operator'     => 'in',
      'value'        => array(
        'atrium_crm_set' => 'atrium_crm_set',
      ),
      'group'        => '0',
      'exposed'      => FALSE,
      'expose'       => array(
        'operator' => FALSE,
        'label'    => '',
      ),
      'id'           => 'type',
      'table'        => 'node',
      'field'        => 'type',
      'relationship' => 'none',
    ),
    'status'                                    => array(
      'operator'     => '=',
      'value'        => '1',
      'group'        => '0',
      'exposed'      => FALSE,
      'expose'       => array(
        'operator' => FALSE,
        'label'    => '',
      ),
      'id'           => 'status',
      'table'        => 'node',
      'field'        => 'status',
      'relationship' => 'none',
    ),
    'title'                                     => array(
      'operator'     => 'contains',
      'value'        => '',
      'group'        => '0',
      'exposed'      => TRUE,
      'expose'       => array(
        'use_operator' => 0,
        'operator'     => 'title_op',
        'identifier'   => 'contact_name',
        'label'        => 'Contact',
        'optional'     => 1,
        'remember'     => 0,
      ),
      'case'         => 0,
      'id'           => 'title',
      'table'        => 'node',
      'field'        => 'title',
      'relationship' => 'field_set_contacts_nid',
    ),
    'field_contact_member_uid'                  => array(
      'operator'          => 'or',
      'value'             => array(),
      'group'             => '0',
      'exposed'           => TRUE,
      'expose'            => array(
        'use_operator' => 0,
        'operator'     => 'field_contact_member_uid_op',
        'identifier'   => 'contact_uid',
        'label'        => 'Member',
        'optional'     => 1,
        'single'       => 1,
        'remember'     => 0,
        'reduce'       => 0,
      ),
      'id'                => 'field_contact_member_uid',
      'table'             => 'node_data_field_contact_member',
      'field'             => 'field_contact_member_uid',
      'override'          => array(
        'button' => 'Override',
      ),
      'relationship'      => 'field_set_contacts_nid',
      'reduce_duplicates' => 0,
    ),
    'city'                                      => array(
      'operator'     => '=',
      'value'        => '',
      'group'        => '0',
      'exposed'      => TRUE,
      'expose'       => array(
        'use_operator' => 0,
        'operator'     => 'city_op',
        'identifier'   => 'city',
        'label'        => 'City',
        'optional'     => 1,
        'remember'     => 0,
      ),
      'case'         => 1,
      'id'           => 'city',
      'table'        => 'location',
      'field'        => 'city',
      'relationship' => 'field_set_contacts_nid',
    ),
    'country'                                   => array(
      'operator'     => 'IS',
      'value'        => array(),
      'group'        => '0',
      'exposed'      => TRUE,
      'expose'       => array(
        'use_operator' => 0,
        'operator'     => 'country_op',
        'identifier'   => 'country',
        'label'        => 'Country',
        'optional'     => 1,
        'single'       => 1,
        'remember'     => 0,
      ),
      'id'           => 'country',
      'table'        => 'location',
      'field'        => 'country',
      'relationship' => 'field_set_contacts_nid',
    ),
    'postal_code'                               => array(
      'operator'     => '=',
      'value'        => '',
      'group'        => '0',
      'exposed'      => TRUE,
      'expose'       => array(
        'use_operator' => 0,
        'operator'     => 'postal_code_op',
        'identifier'   => 'postal_code',
        'label'        => 'Postal Code',
        'optional'     => 1,
        'remember'     => 0,
      ),
      'case'         => 0,
      'id'           => 'postal_code',
      'table'        => 'location',
      'field'        => 'postal_code',
      'relationship' => 'field_set_contacts_nid',
    ),
    'field_contact_opted_out_value_many_to_one' => array(
      'operator'          => 'not',
      'value'             => array(),
      'group'             => '0',
      'exposed'           => TRUE,
      'expose'            => array(
        'use_operator' => 0,
        'operator'     => 'field_contact_opted_out_value_many_to_one_op',
        'identifier'   => 'opted_out',
        'label'        => 'Opted in',
        'optional'     => 1,
        'single'       => 0,
        'remember'     => 0,
        'reduce'       => 0,
      ),
      'id'                => 'field_contact_opted_out_value_many_to_one',
      'table'             => 'node_data_field_contact_opted_out',
      'field'             => 'field_contact_opted_out_value_many_to_one',
      'relationship'      => 'field_set_contacts_nid',
      'reduce_duplicates' => 0,
    ),
  ));
  $handler->override_option('access', array(
    'type'           => 'spaces_feature',
    'spaces_feature' => 'atrium_crm_sets',
    'perm'           => 'access all sets',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'Contact Sets');
  $handler->override_option('header', '<h2 class="title">Sets</h2>');
  $handler->override_option('header_format', '4');
  $handler->override_option('header_empty', 1);
  $handler->override_option('empty', 'No sets found with current filters');
  $handler->override_option('empty_format', '3');
  $handler->override_option('use_ajax', TRUE);
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky'   => 0,
    'order'    => 'asc',
    'columns'  => array(
      'title'     => 'title',
      'teaser'    => 'teaser',
      'edit_node' => 'edit_node',
    ),
    'info'     => array(
      'title'     => array(
        'sortable'  => 1,
        'separator' => '',
      ),
      'teaser'    => array(
        'separator' => '',
      ),
      'edit_node' => array(
        'separator' => '',
      ),
    ),
    'default'  => '-1',
  ));
  $handler->override_option('exposed_block', TRUE);
  $handler = $view->new_display('page', 'Overview', 'page_1');
  $handler->override_option('path', 'contact_sets');
  $handler->override_option('menu', array(
    'type'        => 'normal',
    'title'       => 'Sets',
    'description' => 'Show all sets',
    'weight'      => '1',
    'name'        => 'features',
  ));
  $handler->override_option('tab_options', array(
    'type'        => 'normal',
    'title'       => 'CRM',
    'description' => '',
    'weight'      => '0',
    'name'        => 'features',
  ));
  $handler = $view->new_display('page', 'Contacts', 'page_2');
  $handler->override_option('fields', array(
    'title'                               => array(
      'label'        => 'Full name',
      'alter'        => array(
        'alter_text'    => 0,
        'text'          => '',
        'make_link'     => 0,
        'path'          => '',
        'link_class'    => '',
        'alt'           => '',
        'prefix'        => '',
        'suffix'        => '',
        'target'        => '',
        'help'          => '',
        'trim'          => 0,
        'max_length'    => '',
        'word_boundary' => 1,
        'ellipsis'      => 1,
        'html'          => 0,
        'strip_tags'    => 0,
      ),
      'empty'        => '',
      'hide_empty'   => 0,
      'empty_zero'   => 0,
      'link_to_node' => 1,
      'spaces'       => array(
        'frontpage' => 0,
        'type'      => 'spaces_og',
      ),
      'exclude'      => 0,
      'id'           => 'title',
      'table'        => 'node',
      'field'        => 'title',
      'override'     => array(
        'button' => 'Use default',
      ),
      'relationship' => 'field_set_contacts_nid',
    ),
    'field_contact_email_addresses_email' => array(
      'label'        => 'Email address',
      'alter'        => array(
        'alter_text'    => 0,
        'text'          => '',
        'make_link'     => 0,
        'path'          => '',
        'link_class'    => '',
        'alt'           => '',
        'prefix'        => '',
        'suffix'        => '',
        'target'        => '',
        'help'          => '',
        'trim'          => 0,
        'max_length'    => '',
        'word_boundary' => 1,
        'ellipsis'      => 1,
        'html'          => 0,
        'strip_tags'    => 0,
      ),
      'empty'        => '',
      'hide_empty'   => 0,
      'empty_zero'   => 0,
      'link_to_node' => 0,
      'label_type'   => 'custom',
      'format'       => 'default',
      'multiple'     => array(
        'group'             => 1,
        'multiple_number'   => '1',
        'multiple_from'     => '0',
        'multiple_reversed' => 0,
      ),
      'exclude'      => 0,
      'id'           => 'field_contact_email_addresses_email',
      'table'        => 'node_data_field_contact_email_addresses',
      'field'        => 'field_contact_email_addresses_email',
      'override'     => array(
        'button' => 'Use default',
      ),
      'relationship' => 'field_set_contacts_nid',
    ),
    'city'                                => array(
      'label'        => 'City',
      'alter'        => array(
        'alter_text'    => 0,
        'text'          => '',
        'make_link'     => 0,
        'path'          => '',
        'link_class'    => '',
        'alt'           => '',
        'prefix'        => '',
        'suffix'        => '',
        'target'        => '',
        'help'          => '',
        'trim'          => 0,
        'max_length'    => '',
        'word_boundary' => 1,
        'ellipsis'      => 1,
        'html'          => 0,
        'strip_tags'    => 0,
      ),
      'empty'        => '',
      'hide_empty'   => 0,
      'empty_zero'   => 0,
      'exclude'      => 0,
      'id'           => 'city',
      'table'        => 'location',
      'field'        => 'city',
      'override'     => array(
        'button' => 'Use default',
      ),
      'relationship' => 'field_set_contacts_nid',
    ),
  ));
  $handler->override_option('arguments', array(
    'nid' => array(
      'default_action'                    => 'default',
      'style_plugin'                      => 'default_summary',
      'style_options'                     => array(),
      'wildcard'                          => 'all',
      'wildcard_substitution'             => 'All',
      'title'                             => 'Contacts of set: %1',
      'breadcrumb'                        => '',
      'default_argument_type'             => 'node',
      'default_argument'                  => '',
      'validate_type'                     => 'node',
      'validate_fail'                     => 'not found',
      'break_phrase'                      => 0,
      'not'                               => 0,
      'id'                                => 'nid',
      'table'                             => 'node',
      'field'                             => 'nid',
      'validate_user_argument_type'       => 'uid',
      'validate_user_roles'               => array(
        '2' => 0,
        '3' => 0,
        '4' => 0,
      ),
      'override'                          => array(
        'button' => 'Use default',
      ),
      'relationship'                      => 'none',
      'default_options_div_prefix'        => '',
      'default_argument_fixed'            => '',
      'default_argument_user'             => 0,
      'default_argument_php'              => '',
      'validate_argument_node_type'       => array(
        'atrium_crm_set'            => 'atrium_crm_set',
        'atrium_crm_contact'        => 0,
        'blog'                      => 0,
        'book'                      => 0,
        'event'                     => 0,
        'feed_ical'                 => 0,
        'feed_ical_item'            => 0,
        'group'                     => 0,
        'profile'                   => 0,
        'shoutbox'                  => 0,
        'atrium_crm_account'        => 0,
        'casetracker_basic_case'    => 0,
        'casetracker_basic_project' => 0,
      ),
      'validate_argument_node_access'     => 1,
      'validate_argument_nid_type'        => 'nid',
      'validate_argument_vocabulary'      => array(
        '3' => 0,
        '4' => 0,
        '5' => 0,
        '1' => 0,
        '2' => 0,
      ),
      'validate_argument_type'            => 'tid',
      'validate_argument_transform'       => 0,
      'validate_user_restrict_roles'      => 0,
      'validate_argument_is_member'       => 'OG_VIEWS_DO_NOT_VALIDATE_MEMBERSHIP',
      'validate_argument_group_node_type' => array(
        'group' => 0,
      ),
      'validate_argument_php'             => '',
    ),
  ));
  $handler->override_option('filters', array(
    'status' => array(
      'operator'     => '=',
      'value'        => '1',
      'group'        => '0',
      'exposed'      => FALSE,
      'expose'       => array(
        'operator' => FALSE,
        'label'    => '',
      ),
      'id'           => 'status',
      'table'        => 'node',
      'field'        => 'status',
      'override'     => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
    'type'   => array(
      'operator'     => 'in',
      'value'        => array(
        'atrium_crm_set' => 'atrium_crm_set',
      ),
      'group'        => '0',
      'exposed'      => FALSE,
      'expose'       => array(
        'operator' => FALSE,
        'label'    => '',
      ),
      'id'           => 'type',
      'table'        => 'node',
      'field'        => 'type',
      'override'     => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('title', 'Contacts of Set');
  $handler->override_option('header', '');
  $handler->override_option('header_format', '6');
  $handler->override_option('header_empty', 0);
  $handler->override_option('items_per_page', 0);
  $handler->override_option('use_pager', '0');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky'   => 0,
    'order'    => 'asc',
    'columns'  => array(
      'title'                               => 'title',
      'field_contact_email_addresses_email' => 'field_contact_email_addresses_email',
      'city'                                => 'city',
    ),
    'info'     => array(
      'title'                               => array(
        'sortable'  => 1,
        'separator' => '',
      ),
      'field_contact_email_addresses_email' => array(
        'separator' => '',
      ),
      'city'                                => array(
        'sortable'  => 1,
        'separator' => '',
      ),
    ),
    'default'  => '-1',
  ));
  $handler->override_option('exposed_block', FALSE);
  $handler->override_option('path', 'node/%/contacts');
  $handler->override_option('menu', array(
    'type'        => 'tab',
    'title'       => 'Contacts',
    'description' => '',
    'weight'      => '0',
    'name'        => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type'        => 'none',
    'title'       => '',
    'description' => '',
    'weight'      => 0,
    'name'        => 'navigation',
  ));
  $handler = $view->new_display('attachment', 'Attachment', 'attachment_1');
  $handler->override_option('fields', array(
    'title'                               => array(
      'label'        => '',
      'alter'        => array(
        'alter_text'    => 0,
        'text'          => '',
        'make_link'     => 0,
        'path'          => '',
        'link_class'    => '',
        'alt'           => '',
        'prefix'        => '',
        'suffix'        => '',
        'target'        => '',
        'help'          => '',
        'trim'          => 0,
        'max_length'    => '',
        'word_boundary' => 1,
        'ellipsis'      => 1,
        'html'          => 0,
        'strip_tags'    => 0,
      ),
      'empty'        => '',
      'hide_empty'   => 0,
      'empty_zero'   => 0,
      'link_to_node' => 0,
      'spaces'       => array(
        'frontpage' => 0,
        'type'      => 'spaces_og',
      ),
      'exclude'      => 0,
      'id'           => 'title',
      'table'        => 'node',
      'field'        => 'title',
      'override'     => array(
        'button' => 'Use default',
      ),
      'relationship' => 'field_set_contacts_nid',
    ),
    'field_contact_email_addresses_email' => array(
      'label'        => '',
      'alter'        => array(
        'alter_text'    => 0,
        'text'          => '"[title]" <[field_contact_email_addresses_email]>',
        'make_link'     => 0,
        'path'          => '',
        'link_class'    => '',
        'alt'           => '',
        'prefix'        => '',
        'suffix'        => '',
        'target'        => '',
        'help'          => '',
        'trim'          => 0,
        'max_length'    => '',
        'word_boundary' => 1,
        'ellipsis'      => 1,
        'html'          => 0,
        'strip_tags'    => 0,
      ),
      'empty'        => '',
      'hide_empty'   => 0,
      'empty_zero'   => 0,
      'link_to_node' => 0,
      'label_type'   => 'none',
      'format'       => 'default',
      'multiple'     => array(
        'group'             => 1,
        'multiple_number'   => '1',
        'multiple_from'     => '0',
        'multiple_reversed' => 0,
      ),
      'exclude'      => 0,
      'id'           => 'field_contact_email_addresses_email',
      'table'        => 'node_data_field_contact_email_addresses',
      'field'        => 'field_contact_email_addresses_email',
      'override'     => array(
        'button' => 'Use default',
      ),
      'relationship' => 'field_set_contacts_nid',
    ),
  ));
  $handler->override_option('arguments', array(
    'nid' => array(
      'default_action'                    => 'default',
      'style_plugin'                      => 'default_summary',
      'style_options'                     => array(),
      'wildcard'                          => 'all',
      'wildcard_substitution'             => 'All',
      'title'                             => '',
      'breadcrumb'                        => '',
      'default_argument_type'             => 'node',
      'default_argument'                  => '',
      'validate_type'                     => 'none',
      'validate_fail'                     => 'not found',
      'break_phrase'                      => 0,
      'not'                               => 0,
      'id'                                => 'nid',
      'table'                             => 'node',
      'field'                             => 'nid',
      'validate_user_argument_type'       => 'uid',
      'validate_user_roles'               => array(
        '2' => 0,
        '3' => 0,
        '4' => 0,
      ),
      'override'                          => array(
        'button' => 'Use default',
      ),
      'relationship'                      => 'none',
      'default_options_div_prefix'        => '',
      'default_argument_fixed'            => '',
      'default_argument_user'             => 0,
      'default_argument_php'              => '',
      'validate_argument_node_type'       => array(
        'atrium_crm_contact'        => 0,
        'blog'                      => 0,
        'book'                      => 0,
        'event'                     => 0,
        'feed_ical'                 => 0,
        'feed_ical_item'            => 0,
        'group'                     => 0,
        'profile'                   => 0,
        'shoutbox'                  => 0,
        'atrium_crm_account'        => 0,
        'atrium_crm_set'            => 0,
        'casetracker_basic_case'    => 0,
        'casetracker_basic_project' => 0,
      ),
      'validate_argument_node_access'     => 0,
      'validate_argument_nid_type'        => 'nid',
      'validate_argument_vocabulary'      => array(
        '3' => 0,
        '4' => 0,
        '5' => 0,
        '1' => 0,
        '2' => 0,
      ),
      'validate_argument_type'            => 'tid',
      'validate_argument_transform'       => 0,
      'validate_user_restrict_roles'      => 0,
      'validate_argument_is_member'       => 'OG_VIEWS_DO_NOT_VALIDATE_MEMBERSHIP',
      'validate_argument_group_node_type' => array(
        'group' => 0,
      ),
      'validate_argument_php'             => '',
    ),
  ));
  $handler->override_option('filters', array(
    'type'                                => array(
      'operator'     => 'in',
      'value'        => array(
        'atrium_crm_set' => 'atrium_crm_set',
      ),
      'group'        => '0',
      'exposed'      => FALSE,
      'expose'       => array(
        'operator' => FALSE,
        'label'    => '',
      ),
      'id'           => 'type',
      'table'        => 'node',
      'field'        => 'type',
      'relationship' => 'none',
    ),
    'status'                              => array(
      'operator'     => '=',
      'value'        => '1',
      'group'        => '0',
      'exposed'      => FALSE,
      'expose'       => array(
        'operator' => FALSE,
        'label'    => '',
      ),
      'id'           => 'status',
      'table'        => 'node',
      'field'        => 'status',
      'relationship' => 'none',
    ),
    'field_contact_email_addresses_email' => array(
      'operator'     => 'not empty',
      'value'        => '',
      'group'        => '0',
      'exposed'      => FALSE,
      'expose'       => array(
        'operator' => FALSE,
        'label'    => '',
      ),
      'case'         => 1,
      'id'           => 'field_contact_email_addresses_email',
      'table'        => 'node_data_field_contact_email_addresses',
      'field'        => 'field_contact_email_addresses_email',
      'override'     => array(
        'button' => 'Use default',
      ),
      'relationship' => 'field_set_contacts_nid',
    ),
  ));
  $handler->override_option('header', '');
  $handler->override_option('header_empty', 0);
  $handler->override_option('items_per_page', 0);
  $handler->override_option('style_plugin', 'default');
  $handler->override_option('style_options', array(
    'grouping' => '',
  ));
  $handler->override_option('row_options', array(
    'inline'     => array(),
    'separator'  => ',',
    'hide_empty' => 0,
  ));
  $handler->override_option('attachment_position', 'before');
  $handler->override_option('inherit_arguments', TRUE);
  $handler->override_option('inherit_exposed_filters', FALSE);
  $handler->override_option('inherit_pager', FALSE);
  $handler->override_option('render_pager', TRUE);
  $handler->override_option('displays', array(
    'default' => 0,
    'page_1'  => 0,
    'page_2'  => 0,
  ));
  $handler = $view->new_display('block', 'Block', 'block_1');
  $handler->override_option('fields', array(
    'title'                               => array(
      'label'        => 'Title',
      'alter'        => array(
        'alter_text'    => 0,
        'text'          => '',
        'make_link'     => 0,
        'path'          => '',
        'link_class'    => '',
        'alt'           => '',
        'prefix'        => '',
        'suffix'        => '',
        'target'        => '',
        'help'          => '',
        'trim'          => 0,
        'max_length'    => '',
        'word_boundary' => 1,
        'ellipsis'      => 1,
        'html'          => 0,
        'strip_tags'    => 0,
      ),
      'empty'        => '',
      'hide_empty'   => 0,
      'empty_zero'   => 0,
      'link_to_node' => 1,
      'spaces'       => array(
        'frontpage' => 0,
        'type'      => 'spaces_og',
      ),
      'exclude'      => 0,
      'id'           => 'title',
      'table'        => 'node',
      'field'        => 'title',
      'override'     => array(
        'button' => 'Use default',
      ),
      'relationship' => 'field_set_contacts_nid',
    ),
    'field_contact_email_addresses_email' => array(
      'label'        => 'Email addresses',
      'alter'        => array(
        'alter_text'    => 0,
        'text'          => '',
        'make_link'     => 0,
        'path'          => '',
        'link_class'    => '',
        'alt'           => '',
        'prefix'        => '',
        'suffix'        => '',
        'target'        => '',
        'help'          => '',
        'trim'          => 0,
        'max_length'    => '',
        'word_boundary' => 1,
        'ellipsis'      => 1,
        'html'          => 0,
        'strip_tags'    => 0,
      ),
      'empty'        => '',
      'hide_empty'   => 0,
      'empty_zero'   => 0,
      'link_to_node' => 0,
      'label_type'   => 'widget',
      'format'       => 'default',
      'multiple'     => array(
        'group'             => 1,
        'multiple_number'   => '1',
        'multiple_from'     => '0',
        'multiple_reversed' => 0,
      ),
      'exclude'      => 0,
      'id'           => 'field_contact_email_addresses_email',
      'table'        => 'node_data_field_contact_email_addresses',
      'field'        => 'field_contact_email_addresses_email',
      'override'     => array(
        'button' => 'Use default',
      ),
      'relationship' => 'field_set_contacts_nid',
    ),
  ));
  $handler->override_option('arguments', array(
    'nid' => array(
      'default_action'                    => 'default',
      'style_plugin'                      => 'default_summary',
      'style_options'                     => array(),
      'wildcard'                          => 'all',
      'wildcard_substitution'             => 'All',
      'title'                             => '',
      'breadcrumb'                        => '',
      'default_argument_type'             => 'node',
      'default_argument'                  => '',
      'validate_type'                     => 'none',
      'validate_fail'                     => 'not found',
      'break_phrase'                      => 0,
      'not'                               => 0,
      'id'                                => 'nid',
      'table'                             => 'node',
      'field'                             => 'nid',
      'validate_user_argument_type'       => 'uid',
      'validate_user_roles'               => array(
        '2' => 0,
        '3' => 0,
        '4' => 0,
      ),
      'override'                          => array(
        'button' => 'Override',
      ),
      'relationship'                      => 'none',
      'default_options_div_prefix'        => '',
      'default_argument_fixed'            => '',
      'default_argument_user'             => 0,
      'default_argument_php'              => '',
      'validate_argument_node_type'       => array(
        'atrium_crm_contact'        => 0,
        'blog'                      => 0,
        'book'                      => 0,
        'event'                     => 0,
        'feed_ical'                 => 0,
        'feed_ical_item'            => 0,
        'group'                     => 0,
        'profile'                   => 0,
        'shoutbox'                  => 0,
        'atrium_crm_account'        => 0,
        'atrium_crm_set'            => 0,
        'casetracker_basic_case'    => 0,
        'casetracker_basic_project' => 0,
      ),
      'validate_argument_node_access'     => 0,
      'validate_argument_nid_type'        => 'nid',
      'validate_argument_vocabulary'      => array(
        '3' => 0,
        '4' => 0,
        '5' => 0,
        '1' => 0,
        '2' => 0,
      ),
      'validate_argument_type'            => 'tid',
      'validate_argument_transform'       => 0,
      'validate_user_restrict_roles'      => 0,
      'validate_argument_is_member'       => 'OG_VIEWS_DO_NOT_VALIDATE_MEMBERSHIP',
      'validate_argument_group_node_type' => array(
        'group' => 0,
      ),
      'validate_argument_php'             => '',
    ),
  ));
  $handler->override_option('filters', array(
    'type'                                => array(
      'operator'     => 'in',
      'value'        => array(
        'atrium_crm_set' => 'atrium_crm_set',
      ),
      'group'        => '0',
      'exposed'      => FALSE,
      'expose'       => array(
        'operator' => FALSE,
        'label'    => '',
      ),
      'id'           => 'type',
      'table'        => 'node',
      'field'        => 'type',
      'relationship' => 'none',
    ),
    'status'                              => array(
      'operator'     => '=',
      'value'        => '1',
      'group'        => '0',
      'exposed'      => FALSE,
      'expose'       => array(
        'operator' => FALSE,
        'label'    => '',
      ),
      'id'           => 'status',
      'table'        => 'node',
      'field'        => 'status',
      'relationship' => 'none',
    ),
    'field_contact_email_addresses_email' => array(
      'operator'     => 'not empty',
      'value'        => '',
      'group'        => '0',
      'exposed'      => FALSE,
      'expose'       => array(
        'operator' => FALSE,
        'label'    => '',
      ),
      'case'         => 1,
      'id'           => 'field_contact_email_addresses_email',
      'table'        => 'node_data_field_contact_email_addresses',
      'field'        => 'field_contact_email_addresses_email',
      'override'     => array(
        'button' => 'Use default',
      ),
      'relationship' => 'field_set_contacts_nid',
    ),
  ));
  $handler->override_option('items_per_page', 0);
  $handler->override_option('block_description', '');
  $handler->override_option('block_caching', -1);
  $translatables['atrium_crm_sets'] = array(
    t('<h2 class="title">Sets</h2>'),
    t('Attachment'),
    t('Block'),
    t('Contact Sets'),
    t('Contacts'),
    t('Contacts of Set'),
    t('Defaults'),
    t('No sets found with current filters'),
    t('Overview'),
  );

  $views[$view->name] = $view;

  return $views;
}
