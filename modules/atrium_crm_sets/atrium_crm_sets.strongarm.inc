<?php

/**
 * Implementation of hook_strongarm().
 */
function atrium_crm_sets_strongarm() {
  $export = array();

  $strongarm                    = new stdClass;
  $strongarm->disabled          = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version       = 1;
  $strongarm->name              = 'ant_atrium_crm_set';
  $strongarm->value             = '0';
  $export['ant_atrium_crm_set'] = $strongarm;

  $strongarm                            = new stdClass;
  $strongarm->disabled                  = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version               = 1;
  $strongarm->name                      = 'ant_pattern_atrium_crm_set';
  $strongarm->value                     = '';
  $export['ant_pattern_atrium_crm_set'] = $strongarm;

  $strongarm                        = new stdClass;
  $strongarm->disabled              = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version           = 1;
  $strongarm->name                  = 'ant_php_atrium_crm_set';
  $strongarm->value                 = 0;
  $export['ant_php_atrium_crm_set'] = $strongarm;

  $strongarm                                            = new stdClass;
  $strongarm->disabled                                  = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                               = 1;
  $strongarm->name                                      = 'atrium_activity_update_type_atrium_crm_set';
  $strongarm->value                                     = 1;
  $export['atrium_activity_update_type_atrium_crm_set'] = $strongarm;

  $strongarm                                  = new stdClass;
  $strongarm->disabled                        = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                     = 1;
  $strongarm->name                            = 'comment_anonymous_atrium_crm_set';
  $strongarm->value                           = 0;
  $export['comment_anonymous_atrium_crm_set'] = $strongarm;

  $strongarm                        = new stdClass;
  $strongarm->disabled              = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version           = 1;
  $strongarm->name                  = 'comment_atrium_crm_set';
  $strongarm->value                 = '0';
  $export['comment_atrium_crm_set'] = $strongarm;

  $strongarm                                 = new stdClass;
  $strongarm->disabled                       = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                    = 1;
  $strongarm->name                           = 'comment_controls_atrium_crm_set';
  $strongarm->value                          = '3';
  $export['comment_controls_atrium_crm_set'] = $strongarm;

  $strongarm                                     = new stdClass;
  $strongarm->disabled                           = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                        = 1;
  $strongarm->name                               = 'comment_default_mode_atrium_crm_set';
  $strongarm->value                              = '4';
  $export['comment_default_mode_atrium_crm_set'] = $strongarm;

  $strongarm                                      = new stdClass;
  $strongarm->disabled                            = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                         = 1;
  $strongarm->name                                = 'comment_default_order_atrium_crm_set';
  $strongarm->value                               = '1';
  $export['comment_default_order_atrium_crm_set'] = $strongarm;

  $strongarm                                         = new stdClass;
  $strongarm->disabled                               = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                            = 1;
  $strongarm->name                                   = 'comment_default_per_page_atrium_crm_set';
  $strongarm->value                                  = '50';
  $export['comment_default_per_page_atrium_crm_set'] = $strongarm;

  $strongarm                                      = new stdClass;
  $strongarm->disabled                            = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                         = 1;
  $strongarm->name                                = 'comment_form_location_atrium_crm_set';
  $strongarm->value                               = '0';
  $export['comment_form_location_atrium_crm_set'] = $strongarm;

  $strongarm                                = new stdClass;
  $strongarm->disabled                      = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                   = 1;
  $strongarm->name                          = 'comment_preview_atrium_crm_set';
  $strongarm->value                         = '1';
  $export['comment_preview_atrium_crm_set'] = $strongarm;

  $strongarm                                      = new stdClass;
  $strongarm->disabled                            = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                         = 1;
  $strongarm->name                                = 'comment_subject_field_atrium_crm_set';
  $strongarm->value                               = '1';
  $export['comment_subject_field_atrium_crm_set'] = $strongarm;

  $strongarm                               = new stdClass;
  $strongarm->disabled                     = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                  = 1;
  $strongarm->name                         = 'comment_upload_atrium_crm_set';
  $strongarm->value                        = '0';
  $export['comment_upload_atrium_crm_set'] = $strongarm;

  $strongarm                                      = new stdClass;
  $strongarm->disabled                            = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                         = 1;
  $strongarm->name                                = 'comment_upload_images_atrium_crm_set';
  $strongarm->value                               = 'none';
  $export['comment_upload_images_atrium_crm_set'] = $strongarm;

  $strongarm                                      = new stdClass;
  $strongarm->disabled                            = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                         = 1;
  $strongarm->name                                = 'content_extra_weights_atrium_crm_set';
  $strongarm->value                               = array(
    'title'                => '-5',
    'body_field'           => '-4',
    'revision_information' => '1',
    'author'               => '0',
    'options'              => '2',
    'comment_settings'     => '3',
    'menu'                 => '-2',
    'book'                 => '-1',
    'attachments'          => '4',
  );
  $export['content_extra_weights_atrium_crm_set'] = $strongarm;

  $strongarm                                    = new stdClass;
  $strongarm->disabled                          = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                       = 1;
  $strongarm->name                              = 'content_profile_use_atrium_crm_set';
  $strongarm->value                             = 0;
  $export['content_profile_use_atrium_crm_set'] = $strongarm;

  $strongarm                                      = new stdClass;
  $strongarm->disabled                            = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                         = 1;
  $strongarm->name                                = 'enable_revisions_page_atrium_crm_set';
  $strongarm->value                               = 1;
  $export['enable_revisions_page_atrium_crm_set'] = $strongarm;

  $strongarm                                       = new stdClass;
  $strongarm->disabled                             = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                          = 1;
  $strongarm->name                                 = 'itweak_upload_collapse_atrium_crm_set';
  $strongarm->value                                = '0';
  $export['itweak_upload_collapse_atrium_crm_set'] = $strongarm;

  $strongarm                                              = new stdClass;
  $strongarm->disabled                                    = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                 = 1;
  $strongarm->name                                        = 'itweak_upload_comment_display_atrium_crm_set';
  $strongarm->value                                       = '2';
  $export['itweak_upload_comment_display_atrium_crm_set'] = $strongarm;

  $strongarm                                           = new stdClass;
  $strongarm->disabled                                 = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                              = 1;
  $strongarm->name                                     = 'itweak_upload_node_display_atrium_crm_set';
  $strongarm->value                                    = '2';
  $export['itweak_upload_node_display_atrium_crm_set'] = $strongarm;

  $strongarm                                             = new stdClass;
  $strongarm->disabled                                   = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                = 1;
  $strongarm->name                                       = 'itweak_upload_teaser_display_atrium_crm_set';
  $strongarm->value                                      = '0';
  $export['itweak_upload_teaser_display_atrium_crm_set'] = $strongarm;

  $strongarm                                            = new stdClass;
  $strongarm->disabled                                  = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                               = 1;
  $strongarm->name                                      = 'itweak_upload_teaser_images_atrium_crm_set';
  $strongarm->value                                     = '0';
  $export['itweak_upload_teaser_images_atrium_crm_set'] = $strongarm;

  $strongarm                                                     = new stdClass;
  $strongarm->disabled                                           = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                        = 1;
  $strongarm->name                                               = 'itweak_upload_thumbnail_link_comment_atrium_crm_set';
  $strongarm->value                                              = '_default';
  $export['itweak_upload_thumbnail_link_comment_atrium_crm_set'] = $strongarm;

  $strongarm                                                     = new stdClass;
  $strongarm->disabled                                           = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                        = 1;
  $strongarm->name                                               = 'itweak_upload_thumbnail_link_default_atrium_crm_set';
  $strongarm->value                                              = '_default';
  $export['itweak_upload_thumbnail_link_default_atrium_crm_set'] = $strongarm;

  $strongarm                                                  = new stdClass;
  $strongarm->disabled                                        = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                     = 1;
  $strongarm->name                                            = 'itweak_upload_thumbnail_link_node_atrium_crm_set';
  $strongarm->value                                           = '_default';
  $export['itweak_upload_thumbnail_link_node_atrium_crm_set'] = $strongarm;

  $strongarm                                                    = new stdClass;
  $strongarm->disabled                                          = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                       = 1;
  $strongarm->name                                              = 'itweak_upload_thumbnail_link_teaser_atrium_crm_set';
  $strongarm->value                                             = '_default';
  $export['itweak_upload_thumbnail_link_teaser_atrium_crm_set'] = $strongarm;

  $strongarm                                                    = new stdClass;
  $strongarm->disabled                                          = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                       = 1;
  $strongarm->name                                              = 'itweak_upload_thumbnail_link_upload_atrium_crm_set';
  $strongarm->value                                             = '_default';
  $export['itweak_upload_thumbnail_link_upload_atrium_crm_set'] = $strongarm;

  $strongarm                                                       = new stdClass;
  $strongarm->disabled                                             = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                          = 1;
  $strongarm->name                                                 = 'itweak_upload_thumbnail_preset_comment_atrium_crm_set';
  $strongarm->value                                                = '_default';
  $export['itweak_upload_thumbnail_preset_comment_atrium_crm_set'] = $strongarm;

  $strongarm                                                       = new stdClass;
  $strongarm->disabled                                             = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                          = 1;
  $strongarm->name                                                 = 'itweak_upload_thumbnail_preset_default_atrium_crm_set';
  $strongarm->value                                                = '_default';
  $export['itweak_upload_thumbnail_preset_default_atrium_crm_set'] = $strongarm;

  $strongarm                                                    = new stdClass;
  $strongarm->disabled                                          = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                       = 1;
  $strongarm->name                                              = 'itweak_upload_thumbnail_preset_node_atrium_crm_set';
  $strongarm->value                                             = '_default';
  $export['itweak_upload_thumbnail_preset_node_atrium_crm_set'] = $strongarm;

  $strongarm                                                      = new stdClass;
  $strongarm->disabled                                            = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                         = 1;
  $strongarm->name                                                = 'itweak_upload_thumbnail_preset_teaser_atrium_crm_set';
  $strongarm->value                                               = '_default';
  $export['itweak_upload_thumbnail_preset_teaser_atrium_crm_set'] = $strongarm;

  $strongarm                                                      = new stdClass;
  $strongarm->disabled                                            = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                         = 1;
  $strongarm->name                                                = 'itweak_upload_thumbnail_preset_upload_atrium_crm_set';
  $strongarm->value                                               = '_default';
  $export['itweak_upload_thumbnail_preset_upload_atrium_crm_set'] = $strongarm;

  $strongarm                                             = new stdClass;
  $strongarm->disabled                                   = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                = 1;
  $strongarm->name                                       = 'itweak_upload_upload_preview_atrium_crm_set';
  $strongarm->value                                      = 1;
  $export['itweak_upload_upload_preview_atrium_crm_set'] = $strongarm;

  $strongarm                             = new stdClass;
  $strongarm->disabled                   = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                = 1;
  $strongarm->name                       = 'node_options_atrium_crm_set';
  $strongarm->value                      = array(
    0 => 'status',
    1 => 'revision',
  );
  $export['node_options_atrium_crm_set'] = $strongarm;

  $strongarm                                           = new stdClass;
  $strongarm->disabled                                 = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                              = 1;
  $strongarm->name                                     = 'notifications_content_type_atrium_crm_set';
  $strongarm->value                                    = array(
    0 => 'thread',
    1 => 'nodetype',
    2 => 'author',
  );
  $export['notifications_content_type_atrium_crm_set'] = $strongarm;

  $strongarm                                        = new stdClass;
  $strongarm->disabled                              = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                           = 1;
  $strongarm->name                                  = 'notifications_team_type_atrium_crm_set';
  $strongarm->value                                 = array();
  $export['notifications_team_type_atrium_crm_set'] = $strongarm;

  $strongarm                                      = new stdClass;
  $strongarm->disabled                            = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                         = 1;
  $strongarm->name                                = 'og_content_type_usage_atrium_crm_set';
  $strongarm->value                               = 'omitted';
  $export['og_content_type_usage_atrium_crm_set'] = $strongarm;

  $strongarm                              = new stdClass;
  $strongarm->disabled                    = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                 = 1;
  $strongarm->name                        = 'og_max_groups_atrium_crm_set';
  $strongarm->value                       = '';
  $export['og_max_groups_atrium_crm_set'] = $strongarm;

  $strongarm                                 = new stdClass;
  $strongarm->disabled                       = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                    = 1;
  $strongarm->name                           = 'show_diff_inline_atrium_crm_set';
  $strongarm->value                          = 0;
  $export['show_diff_inline_atrium_crm_set'] = $strongarm;

  $strongarm                                     = new stdClass;
  $strongarm->disabled                           = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                        = 1;
  $strongarm->name                               = 'show_preview_changes_atrium_crm_set';
  $strongarm->value                              = 1;
  $export['show_preview_changes_atrium_crm_set'] = $strongarm;

  $strongarm                       = new stdClass;
  $strongarm->disabled             = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version          = 1;
  $strongarm->name                 = 'upload_atrium_crm_set';
  $strongarm->value                = '1';
  $export['upload_atrium_crm_set'] = $strongarm;

  return $export;
}
