<?php

/**
 * Implementation of hook_content_default_fields().
 */
function atrium_crm_sets_content_default_fields() {
  $fields = array();

  // Exported field: field_set_contacts
  $fields['atrium_crm_set-field_set_contacts'] = array(
    'field_name'          => 'field_set_contacts',
    'type_name'           => 'atrium_crm_set',
    'display_settings'    => array(
      'label'  => array(
        'format'  => 'above',
        'exclude' => 0,
      ),
      '5'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'full'   => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '4'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '2'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '3'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'token'  => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active'       => '1',
    'type'                => 'nodereference',
    'required'            => '0',
    'multiple'            => '1',
    'module'              => 'nodereference',
    'active'              => '1',
    'referenceable_types' => array(
      'atrium_crm_contact'        => 'atrium_crm_contact',
      'atrium_crm_account'        => 0,
      'blog'                      => 0,
      'book'                      => 0,
      'casetracker_basic_case'    => 0,
      'event'                     => 0,
      'group'                     => 0,
      'profile'                   => 0,
      'casetracker_basic_project' => 0,
      'atrium_crm_set'            => 0,
      'shoutbox'                  => 0,
      'feed_ical_item'            => 0,
      'feed_ical'                 => 0,
    ),
    'advanced_view'       => '--',
    'advanced_view_args'  => '',
    'widget'              => array(
      'autocomplete_match' => 'contains',
      'size'               => '60',
      'default_value'      => array(
        '0' => array(
          'nid'            => NULL,
          '_error_element' => 'default_value_widget][field_set_contacts][0][nid][nid',
        ),
      ),
      'default_value_php'  => NULL,
      'label'              => 'Contacts',
      'weight'             => '-3',
      'description'        => '',
      'type'               => 'nodereference_autocomplete',
      'module'             => 'nodereference',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Contacts');

  return $fields;
}
