<?php

/**
 * Implementation of hook_content_default_fields().
 */
function atrium_crm_accounts_content_default_fields() {
  $fields = array();

  // Exported field: field_account_addresses
  $fields['atrium_crm_account-field_account_addresses'] = array(
    'field_name'        => 'field_account_addresses',
    'type_name'         => 'atrium_crm_account',
    'display_settings'  => array(
      'label'  => array(
        'format'  => 'above',
        'exclude' => 0,
      ),
      '5'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'full'   => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '4'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '2'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '3'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'token'  => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active'     => '1',
    'type'              => 'location',
    'required'          => '0',
    'multiple'          => '1',
    'module'            => 'location_cck',
    'active'            => '1',
    'location_settings' => array(
      'form'    => array(
        'fields' => array(
          'name'        => array(
            'collect' => '1',
            'default' => '',
            'weight'  => '2',
          ),
          'street'      => array(
            'collect' => '2',
            'default' => '',
            'weight'  => '4',
          ),
          'additional'  => array(
            'collect' => '1',
            'default' => '',
            'weight'  => '6',
          ),
          'city'        => array(
            'collect' => '2',
            'default' => '',
            'weight'  => '8',
          ),
          'province'    => array(
            'collect' => '2',
            'default' => '',
            'weight'  => '10',
          ),
          'postal_code' => array(
            'collect' => '2',
            'default' => '',
            'weight'  => '12',
          ),
          'country'     => array(
            'collect' => '1',
            'default' => 'nl',
            'weight'  => '14',
          ),
          'locpick'     => array(
            'collect' => '0',
            'weight'  => '20',
          ),
        ),
      ),
      'display' => array(
        'hide' => array(
          'name'          => 0,
          'street'        => 0,
          'additional'    => 0,
          'city'          => 0,
          'province'      => 0,
          'postal_code'   => 0,
          'country'       => 0,
          'locpick'       => 0,
          'province_name' => 0,
          'country_name'  => 0,
          'map_link'      => 0,
          'coords'        => 0,
        ),
      ),
    ),
    'gmap_macro'        => '[gmap ]',
    'gmap_marker'       => 'drupal',
    'widget'            => array(
      'default_value'     => array(
        '0' => array(
          'name'                    => '',
          'street'                  => '',
          'additional'              => '',
          'city'                    => '',
          'province'                => '',
          'postal_code'             => '',
          'country'                 => 'us',
          'cck_preview_in_progress' => TRUE,
          'location_settings'       => array(
            'form'    => array(
              'fields' => array(
                'name'                    => array(
                  'default' => '',
                  'collect' => '1',
                  'weight'  => '2',
                ),
                'street'                  => array(
                  'default' => '',
                  'collect' => 1,
                  'weight'  => '4',
                ),
                'additional'              => array(
                  'default' => '',
                  'collect' => '1',
                  'weight'  => '6',
                ),
                'city'                    => array(
                  'default' => '',
                  'collect' => 1,
                  'weight'  => '8',
                ),
                'province'                => array(
                  'default' => '',
                  'collect' => 1,
                  'weight'  => '10',
                ),
                'postal_code'             => array(
                  'default' => '',
                  'collect' => 1,
                  'weight'  => '12',
                ),
                'country'                 => array(
                  'default' => 'us',
                  'collect' => '1',
                  'weight'  => '14',
                ),
                'locpick'                 => array(
                  'default' => array(
                    'user_latitude'  => '',
                    'user_longitude' => '',
                  ),
                  'collect' => '0',
                  'weight'  => '20',
                  'nodiff'  => TRUE,
                ),
                'cck_preview_in_progress' => array(
                  'default' => TRUE,
                ),
                'location_settings'       => array(
                  'default' => array(
                    'form' => array(
                      'fields' => array(
                        'lid'             => array(
                          'default' => FALSE,
                        ),
                        'name'            => array(
                          'default' => '',
                          'collect' => 1,
                          'weight'  => 2,
                        ),
                        'street'          => array(
                          'default' => '',
                          'collect' => 1,
                          'weight'  => 4,
                        ),
                        'additional'      => array(
                          'default' => '',
                          'collect' => 1,
                          'weight'  => 6,
                        ),
                        'city'            => array(
                          'default' => '',
                          'collect' => 0,
                          'weight'  => 8,
                        ),
                        'province'        => array(
                          'default' => '',
                          'collect' => 0,
                          'weight'  => 10,
                        ),
                        'postal_code'     => array(
                          'default' => '',
                          'collect' => 0,
                          'weight'  => 12,
                        ),
                        'country'         => array(
                          'default' => 'us',
                          'collect' => 1,
                          'weight'  => 14,
                        ),
                        'locpick'         => array(
                          'default' => FALSE,
                          'collect' => 1,
                          'weight'  => 20,
                          'nodiff'  => TRUE,
                        ),
                        'latitude'        => array(
                          'default' => 0,
                        ),
                        'longitude'       => array(
                          'default' => 0,
                        ),
                        'source'          => array(
                          'default' => 0,
                        ),
                        'is_primary'      => array(
                          'default' => 0,
                        ),
                        'delete_location' => array(
                          'default' => FALSE,
                          'nodiff'  => TRUE,
                        ),
                      ),
                    ),
                  ),
                ),
                'lid'                     => array(
                  'default' => FALSE,
                ),
                'latitude'                => array(
                  'default' => 0,
                ),
                'longitude'               => array(
                  'default' => 0,
                ),
                'source'                  => array(
                  'default' => 0,
                ),
                'is_primary'              => array(
                  'default' => 0,
                ),
                'delete_location'         => array(
                  'default' => FALSE,
                  'nodiff'  => TRUE,
                ),
              ),
            ),
            'display' => array(
              'hide' => array(
                'name'          => 0,
                'street'        => 0,
                'additional'    => 0,
                'city'          => 0,
                'province'      => 0,
                'postal_code'   => 0,
                'country'       => 0,
                'locpick'       => 0,
                'province_name' => 0,
                'country_name'  => 0,
                'map_link'      => 0,
                'coords'        => 0,
              ),
            ),
          ),
        ),
      ),
      'default_value_php' => NULL,
      'label'             => 'Addresses',
      'weight'            => '4',
      'description'       => '',
      'type'              => 'location',
      'module'            => 'location_cck',
    ),
  );

  // Exported field: field_account_email_addresses
  $fields['atrium_crm_account-field_account_email_addresses'] = array(
    'field_name'       => 'field_account_email_addresses',
    'type_name'        => 'atrium_crm_account',
    'display_settings' => array(
      'label'  => array(
        'format'  => 'above',
        'exclude' => 0,
      ),
      '5'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'full'   => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '4'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '2'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '3'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'token'  => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active'    => '1',
    'type'             => 'email',
    'required'         => '0',
    'multiple'         => '1',
    'module'           => 'email',
    'active'           => '1',
    'widget'           => array(
      'size'              => '60',
      'default_value'     => array(
        '0' => array(
          'email' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label'             => 'Email addresses',
      'weight'            => '5',
      'description'       => '',
      'type'              => 'email_textfield',
      'module'            => 'email',
    ),
  );

  // Exported field: field_account_employees
  $fields['atrium_crm_account-field_account_employees'] = array(
    'field_name'         => 'field_account_employees',
    'type_name'          => 'atrium_crm_account',
    'display_settings'   => array(
      'label'  => array(
        'format'  => 'above',
        'exclude' => 0,
      ),
      '5'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'full'   => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '4'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '2'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '3'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'token'  => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active'      => '1',
    'type'               => 'number_integer',
    'required'           => '0',
    'multiple'           => '0',
    'module'             => 'number',
    'active'             => '1',
    'prefix'             => '',
    'suffix'             => '',
    'min'                => '',
    'max'                => '',
    'allowed_values'     => '',
    'allowed_values_php' => '',
    'widget'             => array(
      'default_value'     => array(
        '0' => array(
          'value'          => '',
          '_error_element' => 'default_value_widget][field_account_employees][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label'             => 'Employees',
      'weight'            => '18',
      'description'       => '',
      'type'              => 'number',
      'module'            => 'number',
    ),
  );

  // Exported field: field_account_notes
  $fields['atrium_crm_account-field_account_notes'] = array(
    'field_name'         => 'field_account_notes',
    'type_name'          => 'atrium_crm_account',
    'display_settings'   => array(
      'label'  => array(
        'format'  => 'above',
        'exclude' => 0,
      ),
      '5'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'full'   => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '4'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '2'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '3'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'token'  => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active'      => '1',
    'type'               => 'text',
    'required'           => '0',
    'multiple'           => '0',
    'module'             => 'text',
    'active'             => '1',
    'text_processing'    => '0',
    'max_length'         => '',
    'allowed_values'     => '',
    'allowed_values_php' => '',
    'widget'             => array(
      'rows'              => '5',
      'size'              => 60,
      'default_value'     => array(
        '0' => array(
          'value'          => '',
          '_error_element' => 'default_value_widget][field_account_notes][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label'             => 'Notes',
      'weight'            => '19',
      'description'       => '',
      'type'              => 'text_textarea',
      'module'            => 'text',
    ),
  );

  // Exported field: field_account_opted_out
  $fields['atrium_crm_account-field_account_opted_out'] = array(
    'field_name'         => 'field_account_opted_out',
    'type_name'          => 'atrium_crm_account',
    'display_settings'   => array(
      'label'  => array(
        'format'  => 'above',
        'exclude' => 0,
      ),
      '5'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'full'   => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '4'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '2'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '3'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'token'  => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active'      => '1',
    'type'               => 'text',
    'required'           => '0',
    'multiple'           => '1',
    'module'             => 'text',
    'active'             => '1',
    'text_processing'    => '0',
    'max_length'         => '',
    'allowed_values'     => 'phone|Phone
mail|Mail
email|Email',
    'allowed_values_php' => '',
    'widget'             => array(
      'default_value'     => array(
        '0' => array(
          'value' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label'             => 'Opted out',
      'weight'            => '5',
      'description'       => '',
      'type'              => 'optionwidgets_buttons',
      'module'            => 'optionwidgets',
    ),
  );

  // Exported field: field_account_phone_numbers
  $fields['atrium_crm_account-field_account_phone_numbers'] = array(
    'field_name'       => 'field_account_phone_numbers',
    'type_name'        => 'atrium_crm_account',
    'display_settings' => array(
      'label'  => array(
        'format'  => 'above',
        'exclude' => 0,
      ),
      '5'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'full'   => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '4'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '2'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '3'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'token'  => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active'    => '1',
    'type'             => 'text',
    'required'         => '0',
    'multiple'         => '1',
    'module'           => 'text',
    'active'           => '1',
    'text_processing'  => '',
    'max_length'       => '20',
    'allowed_values'   => '',
    'widget'           => array(
      'rows'              => 5,
      'size'              => '60',
      'default_value'     => array(
        '0' => array(
          'value'          => '',
          '_error_element' => 'default_value_widget][field_account_phone_numbers][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label'             => 'Phone numbers',
      'weight'            => '6',
      'description'       => '',
      'type'              => 'text_textfield',
      'module'            => 'text',
    ),
  );

  // Exported field: field_account_rating
  $fields['atrium_crm_account-field_account_rating'] = array(
    'field_name'         => 'field_account_rating',
    'type_name'          => 'atrium_crm_account',
    'display_settings'   => array(
      'label'  => array(
        'format'  => 'above',
        'exclude' => 0,
      ),
      '5'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'full'   => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '4'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '2'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '3'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'token'  => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active'      => '1',
    'type'               => 'number_integer',
    'required'           => '0',
    'multiple'           => '0',
    'module'             => 'number',
    'active'             => '1',
    'prefix'             => '',
    'suffix'             => '',
    'min'                => '',
    'max'                => '',
    'allowed_values'     => '0|N/A
1|Worst
2|Bad
3|Average
4|Good
5|Worst',
    'allowed_values_php' => '',
    'widget'             => array(
      'default_value'     => array(
        '0' => array(
          'value' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label'             => 'Rating',
      'weight'            => '17',
      'description'       => '',
      'type'              => 'optionwidgets_select',
      'module'            => 'optionwidgets',
    ),
  );

  // Exported field: field_account_websites
  $fields['atrium_crm_account-field_account_websites'] = array(
    'field_name'       => 'field_account_websites',
    'type_name'        => 'atrium_crm_account',
    'display_settings' => array(
      'label'  => array(
        'format'  => 'above',
        'exclude' => 0,
      ),
      '5'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'full'   => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '4'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '2'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '3'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'token'  => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active'    => '1',
    'type'             => 'link',
    'required'         => '0',
    'multiple'         => '1',
    'module'           => 'link',
    'active'           => '1',
    'attributes'       => array(
      'target' => '_blank',
      'rel'    => '',
      'class'  => '',
      'title'  => '',
    ),
    'display'          => array(
      'url_cutoff' => '80',
    ),
    'url'              => 0,
    'title'            => 'optional',
    'title_value'      => '',
    'enable_tokens'    => 0,
    'validate_url'     => 1,
    'widget'           => array(
      'default_value'     => array(
        '0' => array(
          'title' => '',
          'url'   => '',
        ),
      ),
      'default_value_php' => NULL,
      'label'             => 'Websites',
      'weight'            => '7',
      'description'       => '',
      'type'              => 'link',
      'module'            => 'link',
    ),
  );

  // Exported field: field_contacts
  $fields['atrium_crm_account-field_contacts'] = array(
    'field_name'              => 'field_contacts',
    'type_name'               => 'atrium_crm_account',
    'display_settings'        => array(
      'label'  => array(
        'format'  => 'above',
        'exclude' => 0,
      ),
      '5'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'full'   => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '4'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '2'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      '3'      => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
      'token'  => array(
        'format'  => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active'           => '1',
    'type'                    => 'nodereferrer',
    'required'                => '0',
    'multiple'                => '0',
    'module'                  => 'nodereferrer',
    'active'                  => '1',
    'referrer_types'          => array(
      'atrium_crm_contact'        => 'atrium_crm_contact',
      'atrium_crm_account'        => 0,
      'blog'                      => 0,
      'book'                      => 0,
      'casetracker_basic_case'    => 0,
      'event'                     => 0,
      'group'                     => 0,
      'profile'                   => 0,
      'casetracker_basic_project' => 0,
      'shoutbox'                  => 0,
      'feed_ical_item'            => 0,
      'feed_ical'                 => 0,
    ),
    'referrer_fields'         => array(
      'field_contact_accounts'     => 'field_contact_accounts',
      'field_referenced_book_page' => 0,
    ),
    'referrer_nodes_per_page' => '0',
    'referrer_pager_element'  => '0',
    'referrer_order'          => 'TITLE_ASC',
    'widget'                  => array(
      'label'       => 'Contacts',
      'weight'      => '16',
      'description' => '',
      'type'        => 'nodereferrer_list',
      'module'      => 'nodereferrer',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Addresses');
  t('Contacts');
  t('Email addresses');
  t('Employees');
  t('Notes');
  t('Opted out');
  t('Phone numbers');
  t('Rating');
  t('Websites');

  return $fields;
}
