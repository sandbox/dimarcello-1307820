<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function atrium_crm_accounts_context_default_contexts() {
  $export = array();

  $context                 = new stdClass;
  $context->disabled       = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version    = 3;
  $context->name           = 'atrium_crm_account';
  $context->description    = '';
  $context->tag            = 'Atrium CRM';
  $context->conditions     = array(
    'node' => array(
      'values'  => array(
        'atrium_crm_account' => 'atrium_crm_account',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions      = array(
    'block' => array(
      'blocks' => array(),
      'layout' => 'wide',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Atrium CRM');
  $export['atrium_crm_account'] = $context;

  $context                 = new stdClass;
  $context->disabled       = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version    = 3;
  $context->name           = 'atrium_crm_accounts';
  $context->description    = '';
  $context->tag            = 'Atrium CRM';
  $context->conditions     = array(
    'views' => array(
      'values' => array(
        'atrium_crm_accounts' => 'atrium_crm_accounts',
      ),
    ),
  );
  $context->reactions      = array(
    'block' => array(
      'blocks' => array(
        'views-189a51fecdd5667121884288ecb5a336' => array(
          'module' => 'views',
          'delta'  => '189a51fecdd5667121884288ecb5a336',
          'region' => 'right',
          'weight' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Atrium CRM');
  $export['atrium_crm_accounts'] = $context;

  return $export;
}
