<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function atrium_crm_accounts_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => 3);
  }
  elseif ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function atrium_crm_accounts_node_info() {
  $items = array(
    'atrium_crm_account' => array(
      'name'           => t('Account'),
      'module'         => 'atrium_crm_accounts',
      'description'    => t('An account (also known as a customer, client, buyer, or purchaser) is usually used to refer to a current or potential buyer or user of the products of an individual or organization, called the supplier, seller, or vendor.'),
      'has_title'      => '1',
      'title_label'    => t('Title'),
      'has_body'       => '0',
      'body_label'     => '',
      'min_word_count' => '0',
      'help'           => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function atrium_crm_accounts_views_api() {
  return array(
    'api' => '2',
  );
}
