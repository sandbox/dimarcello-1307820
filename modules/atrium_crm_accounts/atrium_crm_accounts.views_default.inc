<?php

/**
 * Implementation of hook_views_default_views().
 */
function atrium_crm_accounts_views_default_views() {
  $views = array();

  // Exported view: atrium_crm_accounts
  $view               = new view;
  $view->name         = 'atrium_crm_accounts';
  $view->description  = 'Table listing all accounts';
  $view->tag          = 'Atrium CRM';
  $view->view_php     = '';
  $view->base_table   = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version  = 2;
  $view->disabled     = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler            = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'title'                               => array(
      'label'        => 'Name',
      'alter'        => array(
        'alter_text'    => 0,
        'text'          => '',
        'make_link'     => 0,
        'path'          => '',
        'link_class'    => '',
        'alt'           => '',
        'prefix'        => '',
        'suffix'        => '',
        'target'        => '',
        'help'          => '',
        'trim'          => 0,
        'max_length'    => '',
        'word_boundary' => 1,
        'ellipsis'      => 1,
        'html'          => 0,
        'strip_tags'    => 0,
      ),
      'empty'        => '',
      'hide_empty'   => 0,
      'empty_zero'   => 0,
      'link_to_node' => 1,
      'spaces'       => array(
        'frontpage' => 0,
        'type'      => 'spaces_og',
      ),
      'exclude'      => 0,
      'id'           => 'title',
      'table'        => 'node',
      'field'        => 'title',
      'relationship' => 'none',
    ),
    'city'                                => array(
      'label'        => 'City',
      'alter'        => array(
        'alter_text'    => 0,
        'text'          => '',
        'make_link'     => 0,
        'path'          => '',
        'link_class'    => '',
        'alt'           => '',
        'prefix'        => '',
        'suffix'        => '',
        'target'        => '',
        'help'          => '',
        'trim'          => 0,
        'max_length'    => '',
        'word_boundary' => 1,
        'ellipsis'      => 1,
        'html'          => 0,
        'strip_tags'    => 0,
      ),
      'empty'        => '',
      'hide_empty'   => 0,
      'empty_zero'   => 0,
      'exclude'      => 0,
      'id'           => 'city',
      'table'        => 'location',
      'field'        => 'city',
      'relationship' => 'none',
    ),
    'field_account_phone_numbers_value'   => array(
      'label'        => 'Phone number',
      'alter'        => array(
        'alter_text'    => 0,
        'text'          => '',
        'make_link'     => 0,
        'path'          => '',
        'link_class'    => '',
        'alt'           => '',
        'prefix'        => '',
        'suffix'        => '',
        'target'        => '',
        'help'          => '',
        'trim'          => 0,
        'max_length'    => '',
        'word_boundary' => 1,
        'ellipsis'      => 1,
        'html'          => 0,
        'strip_tags'    => 0,
      ),
      'empty'        => '',
      'hide_empty'   => 0,
      'empty_zero'   => 0,
      'link_to_node' => 0,
      'label_type'   => 'custom',
      'format'       => 'default',
      'multiple'     => array(
        'group'             => 1,
        'multiple_number'   => '1',
        'multiple_from'     => '0',
        'multiple_reversed' => 0,
      ),
      'exclude'      => 0,
      'id'           => 'field_account_phone_numbers_value',
      'table'        => 'node_data_field_account_phone_numbers',
      'field'        => 'field_account_phone_numbers_value',
      'relationship' => 'none',
    ),
    'field_account_email_addresses_email' => array(
      'label'        => 'Email address',
      'alter'        => array(
        'alter_text'    => 0,
        'text'          => '',
        'make_link'     => 0,
        'path'          => '',
        'link_class'    => '',
        'alt'           => '',
        'prefix'        => '',
        'suffix'        => '',
        'target'        => '',
        'help'          => '',
        'trim'          => 0,
        'max_length'    => '',
        'word_boundary' => 1,
        'ellipsis'      => 1,
        'html'          => 0,
        'strip_tags'    => 0,
      ),
      'empty'        => '',
      'hide_empty'   => 0,
      'empty_zero'   => 0,
      'link_to_node' => 0,
      'label_type'   => 'custom',
      'format'       => 'default',
      'multiple'     => array(
        'group'             => 1,
        'multiple_number'   => '1',
        'multiple_from'     => '0',
        'multiple_reversed' => 0,
      ),
      'exclude'      => 0,
      'id'           => 'field_account_email_addresses_email',
      'table'        => 'node_data_field_account_email_addresses',
      'field'        => 'field_account_email_addresses_email',
      'relationship' => 'none',
    ),
    'edit_node'                           => array(
      'label'        => 'Edit link',
      'alter'        => array(
        'alter_text'    => 0,
        'text'          => '',
        'make_link'     => 0,
        'path'          => '',
        'link_class'    => '',
        'alt'           => '',
        'prefix'        => '',
        'suffix'        => '',
        'target'        => '',
        'help'          => '',
        'trim'          => 0,
        'max_length'    => '',
        'word_boundary' => 1,
        'ellipsis'      => 1,
        'html'          => 0,
        'strip_tags'    => 0,
      ),
      'empty'        => '',
      'hide_empty'   => 0,
      'empty_zero'   => 0,
      'text'         => '',
      'exclude'      => 0,
      'id'           => 'edit_node',
      'table'        => 'node',
      'field'        => 'edit_node',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('filters', array(
    'type'                                      => array(
      'operator'     => 'in',
      'value'        => array(
        'atrium_crm_account' => 'atrium_crm_account',
      ),
      'group'        => '0',
      'exposed'      => FALSE,
      'expose'       => array(
        'operator' => FALSE,
        'label'    => '',
      ),
      'id'           => 'type',
      'table'        => 'node',
      'field'        => 'type',
      'relationship' => 'none',
    ),
    'status'                                    => array(
      'operator'     => '=',
      'value'        => '1',
      'group'        => '0',
      'exposed'      => FALSE,
      'expose'       => array(
        'operator' => FALSE,
        'label'    => '',
      ),
      'id'           => 'status',
      'table'        => 'node',
      'field'        => 'status',
      'relationship' => 'none',
    ),
    'title'                                     => array(
      'operator'     => 'allwords',
      'value'        => '',
      'group'        => '0',
      'exposed'      => TRUE,
      'expose'       => array(
        'use_operator' => 0,
        'operator'     => 'title_op',
        'identifier'   => 'name',
        'label'        => 'Name',
        'optional'     => 1,
        'remember'     => 0,
      ),
      'case'         => 0,
      'id'           => 'title',
      'table'        => 'node',
      'field'        => 'title',
      'relationship' => 'none',
    ),
    'city'                                      => array(
      'operator'     => '=',
      'value'        => '',
      'group'        => '0',
      'exposed'      => TRUE,
      'expose'       => array(
        'use_operator' => 0,
        'operator'     => 'city_op',
        'identifier'   => 'city',
        'label'        => 'City',
        'optional'     => 1,
        'remember'     => 0,
      ),
      'case'         => 1,
      'id'           => 'city',
      'table'        => 'location',
      'field'        => 'city',
      'relationship' => 'none',
    ),
    'country'                                   => array(
      'operator'     => 'is',
      'value'        => array(),
      'group'        => '0',
      'exposed'      => TRUE,
      'expose'       => array(
        'use_operator' => 0,
        'operator'     => 'country_op',
        'identifier'   => 'country',
        'label'        => 'Country',
        'optional'     => 1,
        'single'       => 1,
        'remember'     => 0,
      ),
      'id'           => 'country',
      'table'        => 'location',
      'field'        => 'country',
      'relationship' => 'none',
    ),
    'postal_code'                               => array(
      'operator'     => '=',
      'value'        => '',
      'group'        => '0',
      'exposed'      => TRUE,
      'expose'       => array(
        'use_operator' => 0,
        'operator'     => 'postal_code_op',
        'identifier'   => 'postal_code',
        'label'        => 'Postal Code',
        'optional'     => 1,
        'remember'     => 0,
      ),
      'case'         => 1,
      'id'           => 'postal_code',
      'table'        => 'location',
      'field'        => 'postal_code',
      'relationship' => 'none',
    ),
    'field_account_phone_numbers_value'         => array(
      'operator'     => 'allwords',
      'value'        => '',
      'group'        => '0',
      'exposed'      => TRUE,
      'expose'       => array(
        'use_operator' => 0,
        'operator'     => 'field_account_phone_numbers_value_op',
        'identifier'   => 'phone_number',
        'label'        => 'Phone number',
        'optional'     => 1,
        'remember'     => 0,
      ),
      'case'         => 0,
      'id'           => 'field_account_phone_numbers_value',
      'table'        => 'node_data_field_account_phone_numbers',
      'field'        => 'field_account_phone_numbers_value',
      'relationship' => 'none',
    ),
    'field_account_email_addresses_email'       => array(
      'operator'     => 'allwords',
      'value'        => '',
      'group'        => '0',
      'exposed'      => TRUE,
      'expose'       => array(
        'use_operator' => 0,
        'operator'     => 'field_account_email_addresses_email_op',
        'identifier'   => 'email_address',
        'label'        => 'Email address',
        'optional'     => 1,
        'remember'     => 0,
      ),
      'case'         => 1,
      'id'           => 'field_account_email_addresses_email',
      'table'        => 'node_data_field_account_email_addresses',
      'field'        => 'field_account_email_addresses_email',
      'relationship' => 'none',
    ),
    'field_account_opted_out_value_many_to_one' => array(
      'operator'          => 'and',
      'value'             => array(),
      'group'             => '0',
      'exposed'           => TRUE,
      'expose'            => array(
        'use_operator' => 0,
        'operator'     => 'field_account_opted_out_value_many_to_one_op',
        'identifier'   => 'opted_out',
        'label'        => 'Opted out',
        'optional'     => 1,
        'single'       => 0,
        'remember'     => 0,
        'reduce'       => 0,
      ),
      'id'                => 'field_account_opted_out_value_many_to_one',
      'table'             => 'node_data_field_account_opted_out',
      'field'             => 'field_account_opted_out_value_many_to_one',
      'relationship'      => 'none',
      'reduce_duplicates' => 0,
    ),
    'field_account_rating_value_many_to_one'    => array(
      'operator'          => 'or',
      'value'             => array(),
      'group'             => '0',
      'exposed'           => TRUE,
      'expose'            => array(
        'use_operator' => 0,
        'operator'     => 'field_account_rating_value_many_to_one_op',
        'identifier'   => 'field_account_rating_value_many_to_one',
        'label'        => 'Rating',
        'optional'     => 1,
        'single'       => 0,
        'remember'     => 0,
        'reduce'       => 0,
      ),
      'id'                => 'field_account_rating_value_many_to_one',
      'table'             => 'node_data_field_account_rating',
      'field'             => 'field_account_rating_value_many_to_one',
      'relationship'      => 'none',
      'reduce_duplicates' => 0,
    ),
  ));
  $handler->override_option('access', array(
    'type'           => 'spaces_feature',
    'spaces_feature' => 'atrium_crm_accounts',
    'perm'           => 'access all accounts',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'Accounts');
  $handler->override_option('css_class', 'atrium-crm');
  $handler->override_option('header', '<h2 class="title"><?php print t(\'Accounts\'); ?></h2>');
  $handler->override_option('header_format', '6');
  $handler->override_option('header_empty', 1);
  $handler->override_option('empty', 'No accounts found with current filters');
  $handler->override_option('empty_format', '3');
  $handler->override_option('use_ajax', TRUE);
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky'   => 0,
    'order'    => 'asc',
    'columns'  => array(
      'title'                               => 'title',
      'city'                                => 'city',
      'field_account_phone_numbers_value'   => 'field_account_phone_numbers_value',
      'field_account_email_addresses_email' => 'field_account_email_addresses_email',
      'edit_node'                           => 'edit_node',
    ),
    'info'     => array(
      'title'                               => array(
        'sortable'  => 1,
        'separator' => '',
      ),
      'city'                                => array(
        'sortable'  => 1,
        'separator' => '',
      ),
      'field_account_phone_numbers_value'   => array(
        'separator' => '',
      ),
      'field_account_email_addresses_email' => array(
        'separator' => '',
      ),
      'edit_node'                           => array(
        'separator' => '',
      ),
    ),
    'default'  => 'title',
  ));
  $handler->override_option('exposed_block', TRUE);
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->override_option('path', 'crm/accounts');
  $handler->override_option('menu', array(
    'type'        => 'default tab',
    'title'       => 'Accounts',
    'description' => 'Show all accounts',
    'weight'      => '0',
    'name'        => 'features',
  ));
  $handler->override_option('tab_options', array(
    'type'        => 'normal',
    'title'       => 'CRM',
    'description' => '',
    'weight'      => '0',
    'name'        => 'features',
  ));
  $translatables['atrium_crm_accounts'] = array(
    t('<h2 class="title"><?php print t(\'Accounts\'); ?></h2>'),
    t('Accounts'),
    t('Defaults'),
    t('No accounts found with current filters'),
    t('Page'),
  );

  $views[$view->name] = $view;

  return $views;
}
