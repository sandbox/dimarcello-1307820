<?php

/**
 * Implementation of hook_taxonomy_default_vocabularies().
 */
function atrium_crm_accounts_taxonomy_default_vocabularies() {
  return array(
    'atrium_crm_account_industry' => array(
      'name'        => 'Account industry',
      'description' => '',
      'help'        => '',
      'relations'   => '1',
      'hierarchy'   => '0',
      'multiple'    => '0',
      'required'    => '0',
      'tags'        => '1',
      'module'      => 'features_atrium_crm_account_industry',
      'weight'      => '0',
      'nodes'       => array(
        'atrium_crm_account' => 'atrium_crm_account',
      ),
    ),
    'atrium_crm_account_type'     => array(
      'name'        => 'Account type',
      'description' => '',
      'help'        => '',
      'relations'   => '1',
      'hierarchy'   => '0',
      'multiple'    => '0',
      'required'    => '0',
      'tags'        => '1',
      'module'      => 'features_atrium_crm_account_type',
      'weight'      => '0',
      'nodes'       => array(
        'atrium_crm_account' => 'atrium_crm_account',
      ),
    ),
  );
}
