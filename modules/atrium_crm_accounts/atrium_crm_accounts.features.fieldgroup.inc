<?php

/**
 * Implementation of hook_fieldgroup_default_groups().
 */
function atrium_crm_accounts_fieldgroup_default_groups() {
  $groups = array();

  // Exported group: group_account_contact
  $groups['atrium_crm_account-group_account_contact'] = array(
    'group_type' => 'standard',
    'type_name'  => 'atrium_crm_account',
    'group_name' => 'group_account_contact',
    'label'      => 'Contact details',
    'settings'   => array(
      'form'    => array(
        'style'       => 'fieldset_collapsible',
        'description' => '',
      ),
      'display' => array(
        'description' => '',
        '5'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        'teaser'      => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        'full'        => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        '4'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        '2'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        '3'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        'token'       => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        'label'       => 'above',
      ),
    ),
    'weight'     => '-3',
    'fields'     => array(
      '0' => 'field_account_addresses',
      '1' => 'field_account_email_addresses',
      '2' => 'field_account_phone_numbers',
      '3' => 'field_account_websites',
    ),
  );

  // Exported group: group_account_miscellaneous
  $groups['atrium_crm_account-group_account_miscellaneous'] = array(
    'group_type' => 'standard',
    'type_name'  => 'atrium_crm_account',
    'group_name' => 'group_account_miscellaneous',
    'label'      => 'Miscellaneous',
    'settings'   => array(
      'form'    => array(
        'style'       => 'fieldset_collapsible',
        'description' => '',
      ),
      'display' => array(
        'description' => '',
        '5'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        'teaser'      => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        'full'        => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        '4'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        '2'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        '3'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        'token'       => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        'label'       => 'above',
      ),
    ),
    'weight'     => '-4',
    'fields'     => array(
      '0' => 'field_contacts',
      '1' => 'field_account_rating',
      '2' => 'field_account_employees',
      '3' => 'field_account_notes',
    ),
  );

  // Exported group: group_account_opting
  $groups['atrium_crm_account-group_account_opting'] = array(
    'group_type' => 'standard',
    'type_name'  => 'atrium_crm_account',
    'group_name' => 'group_account_opting',
    'label'      => 'Opting details',
    'settings'   => array(
      'form'    => array(
        'style'       => 'fieldset_collapsible',
        'description' => '',
      ),
      'display' => array(
        'description' => '',
        '5'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        'teaser'      => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        'full'        => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        '4'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        '2'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        '3'           => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        'token'       => array(
          'format'  => 'fieldset',
          'exclude' => 0,
        ),
        'label'       => 'above',
      ),
    ),
    'weight'     => '-2',
    'fields'     => array(
      '0' => 'field_account_opted_out',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Contact details');
  t('Miscellaneous');
  t('Opting details');

  return $groups;
}
