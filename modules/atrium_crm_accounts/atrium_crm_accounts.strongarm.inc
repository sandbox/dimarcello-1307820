<?php

/**
 * Implementation of hook_strongarm().
 */
function atrium_crm_accounts_strongarm() {
  $export = array();

  $strongarm                        = new stdClass;
  $strongarm->disabled              = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version           = 1;
  $strongarm->name                  = 'ant_atrium_crm_account';
  $strongarm->value                 = '0';
  $export['ant_atrium_crm_account'] = $strongarm;

  $strongarm                                = new stdClass;
  $strongarm->disabled                      = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                   = 1;
  $strongarm->name                          = 'ant_pattern_atrium_crm_account';
  $strongarm->value                         = '';
  $export['ant_pattern_atrium_crm_account'] = $strongarm;

  $strongarm                            = new stdClass;
  $strongarm->disabled                  = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version               = 1;
  $strongarm->name                      = 'ant_php_atrium_crm_account';
  $strongarm->value                     = 0;
  $export['ant_php_atrium_crm_account'] = $strongarm;

  $strongarm                                                = new stdClass;
  $strongarm->disabled                                      = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                   = 1;
  $strongarm->name                                          = 'atrium_activity_update_type_atrium_crm_account';
  $strongarm->value                                         = 1;
  $export['atrium_activity_update_type_atrium_crm_account'] = $strongarm;

  $strongarm                                      = new stdClass;
  $strongarm->disabled                            = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                         = 1;
  $strongarm->name                                = 'comment_anonymous_atrium_crm_account';
  $strongarm->value                               = 0;
  $export['comment_anonymous_atrium_crm_account'] = $strongarm;

  $strongarm                            = new stdClass;
  $strongarm->disabled                  = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version               = 1;
  $strongarm->name                      = 'comment_atrium_crm_account';
  $strongarm->value                     = '0';
  $export['comment_atrium_crm_account'] = $strongarm;

  $strongarm                                     = new stdClass;
  $strongarm->disabled                           = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                        = 1;
  $strongarm->name                               = 'comment_controls_atrium_crm_account';
  $strongarm->value                              = '3';
  $export['comment_controls_atrium_crm_account'] = $strongarm;

  $strongarm                                         = new stdClass;
  $strongarm->disabled                               = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                            = 1;
  $strongarm->name                                   = 'comment_default_mode_atrium_crm_account';
  $strongarm->value                                  = '4';
  $export['comment_default_mode_atrium_crm_account'] = $strongarm;

  $strongarm                                          = new stdClass;
  $strongarm->disabled                                = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                             = 1;
  $strongarm->name                                    = 'comment_default_order_atrium_crm_account';
  $strongarm->value                                   = '1';
  $export['comment_default_order_atrium_crm_account'] = $strongarm;

  $strongarm                                             = new stdClass;
  $strongarm->disabled                                   = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                = 1;
  $strongarm->name                                       = 'comment_default_per_page_atrium_crm_account';
  $strongarm->value                                      = '50';
  $export['comment_default_per_page_atrium_crm_account'] = $strongarm;

  $strongarm                                          = new stdClass;
  $strongarm->disabled                                = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                             = 1;
  $strongarm->name                                    = 'comment_form_location_atrium_crm_account';
  $strongarm->value                                   = '0';
  $export['comment_form_location_atrium_crm_account'] = $strongarm;

  $strongarm                                    = new stdClass;
  $strongarm->disabled                          = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                       = 1;
  $strongarm->name                              = 'comment_preview_atrium_crm_account';
  $strongarm->value                             = '1';
  $export['comment_preview_atrium_crm_account'] = $strongarm;

  $strongarm                                          = new stdClass;
  $strongarm->disabled                                = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                             = 1;
  $strongarm->name                                    = 'comment_subject_field_atrium_crm_account';
  $strongarm->value                                   = '1';
  $export['comment_subject_field_atrium_crm_account'] = $strongarm;

  $strongarm                                   = new stdClass;
  $strongarm->disabled                         = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                      = 1;
  $strongarm->name                             = 'comment_upload_atrium_crm_account';
  $strongarm->value                            = '0';
  $export['comment_upload_atrium_crm_account'] = $strongarm;

  $strongarm                                          = new stdClass;
  $strongarm->disabled                                = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                             = 1;
  $strongarm->name                                    = 'comment_upload_images_atrium_crm_account';
  $strongarm->value                                   = 'none';
  $export['comment_upload_images_atrium_crm_account'] = $strongarm;

  $strongarm                                          = new stdClass;
  $strongarm->disabled                                = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                             = 1;
  $strongarm->name                                    = 'content_extra_weights_atrium_crm_account';
  $strongarm->value                                   = array(
    'title'                => '-5',
    'revision_information' => '1',
    'author'               => '3',
    'options'              => '2',
    'comment_settings'     => '6',
    'menu'                 => '4',
    'taxonomy'             => '-1',
    'book'                 => '7',
    'path'                 => '5',
    'attachments'          => '0',
  );
  $export['content_extra_weights_atrium_crm_account'] = $strongarm;

  $strongarm                                        = new stdClass;
  $strongarm->disabled                              = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                           = 1;
  $strongarm->name                                  = 'content_profile_use_atrium_crm_account';
  $strongarm->value                                 = 0;
  $export['content_profile_use_atrium_crm_account'] = $strongarm;

  $strongarm                                          = new stdClass;
  $strongarm->disabled                                = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                             = 1;
  $strongarm->name                                    = 'enable_revisions_page_atrium_crm_account';
  $strongarm->value                                   = 1;
  $export['enable_revisions_page_atrium_crm_account'] = $strongarm;

  $strongarm                                           = new stdClass;
  $strongarm->disabled                                 = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                              = 1;
  $strongarm->name                                     = 'itweak_upload_collapse_atrium_crm_account';
  $strongarm->value                                    = '0';
  $export['itweak_upload_collapse_atrium_crm_account'] = $strongarm;

  $strongarm                                                  = new stdClass;
  $strongarm->disabled                                        = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                     = 1;
  $strongarm->name                                            = 'itweak_upload_comment_display_atrium_crm_account';
  $strongarm->value                                           = '2';
  $export['itweak_upload_comment_display_atrium_crm_account'] = $strongarm;

  $strongarm                                               = new stdClass;
  $strongarm->disabled                                     = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                  = 1;
  $strongarm->name                                         = 'itweak_upload_node_display_atrium_crm_account';
  $strongarm->value                                        = '2';
  $export['itweak_upload_node_display_atrium_crm_account'] = $strongarm;

  $strongarm                                                 = new stdClass;
  $strongarm->disabled                                       = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                    = 1;
  $strongarm->name                                           = 'itweak_upload_teaser_display_atrium_crm_account';
  $strongarm->value                                          = '0';
  $export['itweak_upload_teaser_display_atrium_crm_account'] = $strongarm;

  $strongarm                                                = new stdClass;
  $strongarm->disabled                                      = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                   = 1;
  $strongarm->name                                          = 'itweak_upload_teaser_images_atrium_crm_account';
  $strongarm->value                                         = '0';
  $export['itweak_upload_teaser_images_atrium_crm_account'] = $strongarm;

  $strongarm                                                         = new stdClass;
  $strongarm->disabled                                               = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                            = 1;
  $strongarm->name                                                   = 'itweak_upload_thumbnail_link_comment_atrium_crm_account';
  $strongarm->value                                                  = '_default';
  $export['itweak_upload_thumbnail_link_comment_atrium_crm_account'] = $strongarm;

  $strongarm                                                         = new stdClass;
  $strongarm->disabled                                               = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                            = 1;
  $strongarm->name                                                   = 'itweak_upload_thumbnail_link_default_atrium_crm_account';
  $strongarm->value                                                  = '_default';
  $export['itweak_upload_thumbnail_link_default_atrium_crm_account'] = $strongarm;

  $strongarm                                                      = new stdClass;
  $strongarm->disabled                                            = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                         = 1;
  $strongarm->name                                                = 'itweak_upload_thumbnail_link_node_atrium_crm_account';
  $strongarm->value                                               = '_default';
  $export['itweak_upload_thumbnail_link_node_atrium_crm_account'] = $strongarm;

  $strongarm                                                        = new stdClass;
  $strongarm->disabled                                              = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                           = 1;
  $strongarm->name                                                  = 'itweak_upload_thumbnail_link_teaser_atrium_crm_account';
  $strongarm->value                                                 = '_default';
  $export['itweak_upload_thumbnail_link_teaser_atrium_crm_account'] = $strongarm;

  $strongarm                                                        = new stdClass;
  $strongarm->disabled                                              = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                           = 1;
  $strongarm->name                                                  = 'itweak_upload_thumbnail_link_upload_atrium_crm_account';
  $strongarm->value                                                 = '_default';
  $export['itweak_upload_thumbnail_link_upload_atrium_crm_account'] = $strongarm;

  $strongarm                                                           = new stdClass;
  $strongarm->disabled                                                 = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                              = 1;
  $strongarm->name                                                     = 'itweak_upload_thumbnail_preset_comment_atrium_crm_account';
  $strongarm->value                                                    = '_default';
  $export['itweak_upload_thumbnail_preset_comment_atrium_crm_account'] = $strongarm;

  $strongarm                                                           = new stdClass;
  $strongarm->disabled                                                 = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                              = 1;
  $strongarm->name                                                     = 'itweak_upload_thumbnail_preset_default_atrium_crm_account';
  $strongarm->value                                                    = '_default';
  $export['itweak_upload_thumbnail_preset_default_atrium_crm_account'] = $strongarm;

  $strongarm                                                        = new stdClass;
  $strongarm->disabled                                              = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                           = 1;
  $strongarm->name                                                  = 'itweak_upload_thumbnail_preset_node_atrium_crm_account';
  $strongarm->value                                                 = '_default';
  $export['itweak_upload_thumbnail_preset_node_atrium_crm_account'] = $strongarm;

  $strongarm                                                          = new stdClass;
  $strongarm->disabled                                                = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                             = 1;
  $strongarm->name                                                    = 'itweak_upload_thumbnail_preset_teaser_atrium_crm_account';
  $strongarm->value                                                   = '_default';
  $export['itweak_upload_thumbnail_preset_teaser_atrium_crm_account'] = $strongarm;

  $strongarm                                                          = new stdClass;
  $strongarm->disabled                                                = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                             = 1;
  $strongarm->name                                                    = 'itweak_upload_thumbnail_preset_upload_atrium_crm_account';
  $strongarm->value                                                   = '_default';
  $export['itweak_upload_thumbnail_preset_upload_atrium_crm_account'] = $strongarm;

  $strongarm                                                 = new stdClass;
  $strongarm->disabled                                       = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                    = 1;
  $strongarm->name                                           = 'itweak_upload_upload_preview_atrium_crm_account';
  $strongarm->value                                          = 1;
  $export['itweak_upload_upload_preview_atrium_crm_account'] = $strongarm;

  $strongarm                                 = new stdClass;
  $strongarm->disabled                       = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                    = 1;
  $strongarm->name                           = 'node_options_atrium_crm_account';
  $strongarm->value                          = array(
    0 => 'status',
    1 => 'revision',
  );
  $export['node_options_atrium_crm_account'] = $strongarm;

  $strongarm                                                           = new stdClass;
  $strongarm->disabled                                                 = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                              = 1;
  $strongarm->name                                                     = 'nodeformscols_field_placements_atrium_crm_account_default';
  $strongarm->value                                                    = array(
    'title'                       => array(
      'region'       => 'main',
      'weight'       => '-5',
      'has_required' => TRUE,
      'title'        => 'Name',
    ),
    'menu'                        => array(
      'region'       => 'right',
      'weight'       => '3',
      'has_required' => FALSE,
      'title'        => 'Menu settings',
      'collapsed'    => 1,
      'hidden'       => 0,
    ),
    'revision_information'        => array(
      'region'       => 'right',
      'weight'       => '5',
      'has_required' => FALSE,
      'title'        => 'Revision information',
      'collapsed'    => 1,
      'hidden'       => 0,
    ),
    'comment_settings'            => array(
      'region'       => 'right',
      'weight'       => '7',
      'has_required' => FALSE,
      'title'        => 'Comment settings',
      'collapsed'    => 1,
      'hidden'       => 0,
    ),
    'options'                     => array(
      'region'       => 'right',
      'weight'       => '6',
      'has_required' => FALSE,
      'title'        => 'Publishing options',
      'collapsed'    => 1,
      'hidden'       => 0,
    ),
    'author'                      => array(
      'region'       => 'right',
      'weight'       => '4',
      'has_required' => FALSE,
      'title'        => 'Authoring information',
      'collapsed'    => 1,
      'hidden'       => 0,
    ),
    'buttons'                     => array(
      'region'       => 'main',
      'weight'       => '100',
      'has_required' => FALSE,
      'title'        => NULL,
      'hidden'       => 0,
    ),
    'notifications'               => array(
      'region'       => 'main',
      'weight'       => '0.014',
      'has_required' => FALSE,
      'title'        => 'Notifications',
      'collapsed'    => 0,
      'hidden'       => 0,
    ),
    'book'                        => array(
      'region'       => 'main',
      'weight'       => '10',
      'has_required' => FALSE,
      'title'        => 'Book outline',
      'collapsed'    => 1,
      'hidden'       => 0,
    ),
    'attachments'                 => array(
      'region'       => 'main',
      'weight'       => '30',
      'has_required' => FALSE,
      'title'        => 'File attachments',
      'collapsed'    => 1,
      'hidden'       => 0,
    ),
    'group_account_miscellaneous' => array(
      'region'       => 'right',
      'weight'       => '0',
      'has_required' => FALSE,
      'title'        => 'Miscellaneous',
      'hidden'       => 0,
      'collapsed'    => 0,
    ),
    'group_account_contact'       => array(
      'region'       => 'main',
      'weight'       => '-2',
      'has_required' => FALSE,
      'title'        => 'Contact details',
      'hidden'       => 0,
      'collapsed'    => 0,
    ),
    'group_account_opting'        => array(
      'region'       => 'right',
      'weight'       => '1',
      'has_required' => FALSE,
      'title'        => 'Opting details',
      'hidden'       => 0,
      'collapsed'    => 0,
    ),
    'taxonomy'                    => array(
      'region'       => 'right',
      'weight'       => '2',
      'has_required' => FALSE,
      'title'        => NULL,
      'hidden'       => 0,
    ),
  );
  $export['nodeformscols_field_placements_atrium_crm_account_default'] = $strongarm;

  $strongarm                                               = new stdClass;
  $strongarm->disabled                                     = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                                  = 1;
  $strongarm->name                                         = 'notifications_content_type_atrium_crm_account';
  $strongarm->value                                        = array(
    0 => 'thread',
    1 => 'nodetype',
    2 => 'author',
  );
  $export['notifications_content_type_atrium_crm_account'] = $strongarm;

  $strongarm                                            = new stdClass;
  $strongarm->disabled                                  = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                               = 1;
  $strongarm->name                                      = 'notifications_team_type_atrium_crm_account';
  $strongarm->value                                     = array();
  $export['notifications_team_type_atrium_crm_account'] = $strongarm;

  $strongarm                                          = new stdClass;
  $strongarm->disabled                                = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                             = 1;
  $strongarm->name                                    = 'og_content_type_usage_atrium_crm_account';
  $strongarm->value                                   = 'omitted';
  $export['og_content_type_usage_atrium_crm_account'] = $strongarm;

  $strongarm                                  = new stdClass;
  $strongarm->disabled                        = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                     = 1;
  $strongarm->name                            = 'og_max_groups_atrium_crm_account';
  $strongarm->value                           = '';
  $export['og_max_groups_atrium_crm_account'] = $strongarm;

  $strongarm                                          = new stdClass;
  $strongarm->disabled                                = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                             = 1;
  $strongarm->name                                    = 'pathauto_node_atrium_crm_account_pattern';
  $strongarm->value                                   = 'crm/accounts/[title-raw]';
  $export['pathauto_node_atrium_crm_account_pattern'] = $strongarm;

  $strongarm                                     = new stdClass;
  $strongarm->disabled                           = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                        = 1;
  $strongarm->name                               = 'show_diff_inline_atrium_crm_account';
  $strongarm->value                              = 0;
  $export['show_diff_inline_atrium_crm_account'] = $strongarm;

  $strongarm                                         = new stdClass;
  $strongarm->disabled                               = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                            = 1;
  $strongarm->name                                   = 'show_preview_changes_atrium_crm_account';
  $strongarm->value                                  = 1;
  $export['show_preview_changes_atrium_crm_account'] = $strongarm;

  $strongarm                           = new stdClass;
  $strongarm->disabled                 = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version              = 1;
  $strongarm->name                     = 'upload_atrium_crm_account';
  $strongarm->value                    = '1';
  $export['upload_atrium_crm_account'] = $strongarm;

  $strongarm                               = new stdClass;
  $strongarm->disabled                     = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                  = 1;
  $strongarm->name                         = 'xref_links_atrium_crm_account';
  $strongarm->value                        = 'template';
  $export['xref_links_atrium_crm_account'] = $strongarm;

  $strongarm                              = new stdClass;
  $strongarm->disabled                    = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version                 = 1;
  $strongarm->name                        = 'xref_view_atrium_crm_account';
  $strongarm->value                       = 0;
  $export['xref_view_atrium_crm_account'] = $strongarm;

  return $export;
}
